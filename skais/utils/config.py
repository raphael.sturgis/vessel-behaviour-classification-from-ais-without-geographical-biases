# -*- coding: utf-8 -*-
"""
.. moduleauthor:: Valentin Emiya
"""
import os
from pathlib import Path
from configparser import ConfigParser


def get_config_file():
    """
    User configuration file

    Returns
    -------
    Path
    """
    return Path(os.path.expanduser('~')) / '.config' / 'skais.conf'


def generate_config():
    """
    Generate an empty configuration file.
    """

    config = ConfigParser(allow_no_value=True)

    config.add_section('DATA')
    config.set('DATA', '# path to data')
    config.set('DATA', 'data_path', '/to/be/completed/skais/data')
    config_file = get_config_file()
    config_file.parent.mkdir(exist_ok=True, parents=True)
    with open(config_file, 'w') as file:
        config.write(file)
    print('Configuration file created: {}. Please update it with your data '
          'path.'.format(config_file))


def get_data_path():
    """
    Read data folder from user configuration file.

    Returns
    -------
    Path
    """
    config_file = get_config_file()
    if not config_file.exists():
        raise Exception('Configuration file does not exists. To create it, '
                        'execute tffpy.utils.generate_config()')
    config = ConfigParser()
    config.read(config_file)
    data_path = Path(config['DATA']['data_path'])
    if not data_path.exists():
        raise Exception('Invalid data path: {}. Update configuration file {}'
                        .format(data_path, config_file))
    return data_path

import numpy as np
from numba import jit

R = 6371000


@jit(nopython=True)
def great_circle(lat1, lat2, long1, long2):
    x1 = np.radians(lat1)
    y1 = np.radians(long1)
    x2 = np.radians(lat2)
    y2 = np.radians(long2)

    delta_x = x2 - x1
    delta_y = y2 - y1

    a = np.sin(delta_x / 2) * np.sin(delta_x / 2) + np.cos(x1) * np.cos(x2) * \
        np.sin(delta_y / 2) * np.sin(delta_y / 2)

    c = 2 * np.arctan2(np.sqrt(a), np.sqrt(1 - a))

    d = R * c
    return d


def get_coord(lat, lon, height, width, min_lat, max_lat, min_lon, max_lon):
    x_coord = max(min(height - int(height * (lat - min_lat) / (max_lat - min_lat)) - 1, height - 1), 0)
    y_coord = max(min(int((width - 1) * (lon - min_lon) / (max_lon - min_lon)), width - 1), 0)

    return x_coord, y_coord


def position_from_distance(position, distances):
    lat = np.arcsin(
        np.sin(np.radians(position[0])) * np.cos(distances[0] / R) + np.cos(np.radians(position[0])) * np.sin(
            distances[0] / R))

    long = np.radians(position[1]) + np.arctan2(
        np.sin(distances[1] / R) * np.cos(np.radians(position[0])),
        np.cos(distances[1] / R) - (np.sin(np.radians(position[1])) * np.sin(lat))
    )

    return np.degrees(lat), np.degrees(long)

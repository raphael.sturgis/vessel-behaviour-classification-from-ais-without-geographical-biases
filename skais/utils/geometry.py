def bresenham(x1, y1, x2, y2):
    dx = int(x2 - x1)
    dy = int(y2 - y1)

    sx = sy = 1
    if dx < 0:
        sx = -1
    if dy < 0:
        sy = -1


    pixels = []
    if abs(dx) > abs(dy): # slope < 1
        if x1 > x2:
            tmp = x2
            x2 = x1
            x1 = tmp

            tmp = y2
            y2 = y1
            y1 = tmp
            sy *= -1

        y = y1
        p = (2 * abs(dy)) - abs(dx)
        pixels.append((x1, y1))
        
        for x in range(x1 + 1, x2 + 1):
            if p < 0:
                p += 2 * abs(dy)
            else:
                y += sy
                p += (2 * abs(dy)) - (2 * abs(dx))
            pixels.append((x, y))
    else: # slope >= 1
        if y1 > y2:
            tmp = x2
            x2 = x1
            x1 = tmp

            tmp = y2
            y2 = y1
            y1 = tmp
            sx *= -1
        x = x1

        pixels.append((x1, y1))
        p = (2 * abs(dx)) - abs(dy)
        for y in range(y1 + 1, y2 + 1):
            if p < 0:
                p += 2 * abs(dx)
            else:
                x += sx
                p += (2 * abs(dx)) - (2 * abs(dy))
            pixels.append((x, y))

    return pixels


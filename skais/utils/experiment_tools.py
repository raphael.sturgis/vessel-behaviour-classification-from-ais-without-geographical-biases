def make_feature_vectors(trajectories, features=None,
                         label_field='label', nb_classes=2, sliding_window_gap=10, length_list=15):
    if features is None:
        features = ['rot', 'sog', 'd_sog', 'd_cog', 'd_heading', 'd_rot']
    x = []
    y = []
    features_trajectories = []
    label_trajectories = []
    zero = [0 for _ in range(nb_classes)]

    for trajectory in trajectories:
        trajectory.df.dropna(inplace=True)
        if len(trajectory.df.index) > length_list:
            trajectory.df['ts'] = trajectory.df.index
            windows = trajectory.sliding_window(length_list, offset=sliding_window_gap,
                                                fields=features + [label_field])

            features_trajectories.append(trajectory.to_numpy(fields=features))
            label_trajectories.append(trajectory.to_numpy(fields=[label_field]).astype(int))

            if length_list > 0:
                for window in windows:
                    label = zero.copy()
                    label[int(window[length_list // 2, -1])] = 1
                    x.append(window[:, :-1])
                    y.append(label)

    return features_trajectories, label_trajectories, x, y

import unittest

from skais.ais.ais_trajectory import *


class TestAISTrajectory(unittest.TestCase):
    def test_to_geojson(self):
        trajectory = AISTrajectory(
            pd.DataFrame(
                {
                    "latitude": [0, 90, 0, -90],
                    "longitude": [0, 90, 180, -90],
                    "ts_sec": [i for i in range(4)]
                }
            )
        )

        expected = {'type': 'LineString', 'coordinates': [[0, 0], [90, 90], [180, 0], [-90, -90]]}

        self.assertDictEqual(trajectory.to_geojson(), expected)

    def test_sliding_window(self):
        trajectory = AISTrajectory(
            pd.DataFrame(
                {
                    "latitude": [0, 90, 0, -90],
                    "longitude": [0, 90, 180, -90],
                    "sog": [i for i in range(4)],
                    "ts_sec": [i for i in range(4)]
                }
            )
        )
        result = trajectory.sliding_window(size=3, offset=1, fields="sog")
        expected = [
            np.array([0, 1, 2]),
            np.array([1, 2, 3])
        ]

        self.assertEqual(len(result), len(expected))

        for r, e in zip(result, expected):
            np.testing.assert_array_equal(r, e)

    def test_sliding_window_too_short(self):
        trajectory = AISTrajectory(
            pd.DataFrame(
                {
                    "latitude": [0, 90, 0, -90],
                    "longitude": [0, 90, 180, -90],
                    "sog": [i for i in range(4)],
                    "ts_sec": [i for i in range(4)]
                }
            )
        )
        result = trajectory.sliding_window(size=5, offset=1, fields="sog")
        expected = []

        self.assertEqual(len(result), len(expected))
        self.assertListEqual(result, expected)

    def test_to_numpy_no_field(self):
        trajectory = AISTrajectory(
            pd.DataFrame(
                {
                    "sog": [i for i in range(4)],
                    "ts_sec": [i for i in range(4)]
                }
            )
        )

        result = trajectory.to_numpy()
        expected = np.stack([np.arange(4), np.arange(4)]).T

        np.testing.assert_array_equal(result, expected)

    def test_to_numpy_field(self):
        trajectory = AISTrajectory(
            pd.DataFrame(
                {
                    "sog": [i for i in range(4)],
                    "ts_sec": [i for i in range(4)]
                }
            )
        )

        result = trajectory.to_numpy("sog")
        expected = np.arange(4)

        np.testing.assert_array_equal(result, expected)

    def test_interpolation(self):
        trajectory = AISTrajectory(
            pd.DataFrame(
                {
                    "latitude": [0 for _ in range(0, 101, 10)],
                    "longitude": [i - 2 for i in range(0, 101, 10)],
                    "sog": [i * 2 for i in range(11)],
                    "ts_sec": [i for i in range(0, 6001, 600)]
                }
            ),
            interpolation_time=300
        )

        expected = pd.DataFrame(
            {
                "ts_sec": [i for i in range(0, 6001, 300)],
                "longitude": [float(i - 2) for i in range(0, 101, 5)],
                "latitude": [0.0 for _ in range(0, 101, 5)],
                "sog": [float(i) for i in range(21)]
            }
        )

        pd.testing.assert_frame_equal(trajectory.df, expected)

    def test_interpolation_discrete(self):
        trajectory = AISTrajectory(
            pd.DataFrame(
                {
                    "ts_sec": [i for i in range(0, 6001, 600)],
                    "label": [0 for _ in range(0, 3001, 600)] + [1 for _ in range(3001, 6001, 600)]
                }
            ),
            interpolation_time=300
        )

        expected = pd.DataFrame(
            {
                "ts_sec": [i for i in range(0, 6001, 300)],
                "label": [0 for _ in range(0, 3301, 300)] + [1 for _ in range(3301, 6001, 300)]
            }
        )

        pd.testing.assert_frame_equal(trajectory.df, expected)

    def test_split_trajectory_simple(self):
        trajectory = AISTrajectory(
            pd.DataFrame(
                {
                    "ts_sec": [i for i in range(0, 3001, 600)] + [i for i in range(4001, 7001, 600)],
                    "label": [0 for _ in range(0, 3001, 600)] + [1 for _ in range(4001, 7001, 600)]
                }
            )
        )

        expected = [
            AISTrajectory(
                pd.DataFrame(
                    {
                        "ts_sec": [i for i in range(0, 3001, 600)],
                        "label": [0 for _ in range(0, 3001, 600)]
                    }
                )
            ),
            AISTrajectory(
                pd.DataFrame(
                    {
                        "ts_sec": [i for i in range(4001, 7001, 600)],
                        "label": [1 for _ in range(4001, 7001, 600)]
                    }
                )
            )
        ]

        for e, r in zip(expected, trajectory.split_trajectory(800)):
            pd.testing.assert_frame_equal(e.df.reset_index(drop=True), r.df.reset_index(drop=True))

    def test_apply_func_on_points_no_column(self):
        trajectory = AISTrajectory(
            pd.DataFrame(
                {
                    "sog": [i for i in range(100)],
                    "ts_sec": [i for i in range(100)]
                }
            )
        )
        expected = pd.DataFrame(
            {
                "sog": [np.sqrt(i) for i in range(100)],
                "ts_sec": [i for i in range(100)]
            }
        )

        trajectory.apply_func_on_points(np.sqrt, 'sog')

        result = trajectory.df

        pd.testing.assert_frame_equal(expected.reset_index(drop=True), result.reset_index(drop=True))

    def test_apply_func_on_points(self):
        trajectory = AISTrajectory(
            pd.DataFrame(
                {
                    "sog": [i for i in range(100)],
                    "ts_sec": [i for i in range(100)]
                }
            )
        )
        expected = pd.DataFrame(
            {
                "sog": [i for i in range(100)],
                "ts_sec": [i for i in range(100)],
                "sqrt_sog": [np.sqrt(i) for i in range(100)]
            }
        )

        trajectory.apply_func_on_points(np.sqrt, 'sog', 'sqrt_sog')

        result = trajectory.df

        pd.testing.assert_frame_equal(expected.reset_index(drop=True), result.reset_index(drop=True))

    def test_apply_func_on_time_sequence_no_column(self):
        def sog_div_ts(p1, _, t1, __):
            return p1 / t1

        trajectory = AISTrajectory(
            pd.DataFrame(
                {
                    "sog": [i + 1 for i in range(100)],
                    "ts_sec": [i + 1 for i in range(100)]
                }
            )
        )
        expected = pd.DataFrame(
            {
                "sog": [1.0 for _ in range(100)],
                "ts_sec": [i + 1 for i in range(100)]
            }
        )

        trajectory.apply_func_on_time_sequence(sog_div_ts, 'sog')

        result = trajectory.df

        pd.testing.assert_frame_equal(expected.reset_index(drop=True), result.reset_index(drop=True))

    def test_apply_func_on_time_sequence(self):
        def sog_div_ts(p1, _, t1, __):
            return p1 / t1

        trajectory = AISTrajectory(
            pd.DataFrame(
                {
                    "sog": [i + 1 for i in range(100)],
                    "ts_sec": [i + 1 for i in range(100)]
                }
            )
        )
        expected = pd.DataFrame(
            {
                "sog": [i + 1 for i in range(100)],
                "ts_sec": [i + 1 for i in range(100)],
                "div": [1.0 for _ in range(100)]
            }
        )

        trajectory.apply_func_on_time_sequence(sog_div_ts, 'sog', 'div')

        result = trajectory.df

        pd.testing.assert_frame_equal(expected.reset_index(drop=True), result.reset_index(drop=True))

    def test_apply_func_on_time_window_no_column(self):
        trajectory = AISTrajectory(
            pd.DataFrame(
                {
                    "sog": [i for i in range(100)],
                    "ts_sec": [i for i in range(100)]
                }
            )
        )
        expected = pd.DataFrame(
            {
                "sog": [0.6, 1.2] +
                       [np.average(np.array([i - 2, i - 1, i, i + 1, i + 2])) for i in range(2, 98)] +
                       [97.8, 98.4],
                "ts_sec": [i for i in range(100)]
            }
        )

        trajectory.apply_func_on_time_window(np.average, 2, 'sog', )

        result = trajectory.df

        pd.testing.assert_frame_equal(expected.reset_index(drop=True), result.reset_index(drop=True))

    def test_apply_func_on_time_window(self):
        trajectory = AISTrajectory(
            pd.DataFrame(
                {
                    "sog": [i for i in range(100)],
                    "ts_sec": [i for i in range(100)]
                }
            )
        )
        expected = pd.DataFrame(
            {
                "sog": [i for i in range(100)],
                "ts_sec": [i for i in range(100)],
                "mean": [0.6, 1.2] + [np.average(np.array([i - 2, i - 1, i, i + 1, i + 2])) for i in range(2, 98)] + [
                    97.8, 98.4]
            }
        )

        trajectory.apply_func_on_time_window(np.average, 2, 'sog', 'mean')

        result = trajectory.df

        pd.testing.assert_frame_equal(expected.reset_index(drop=True), result.reset_index(drop=True))

    def test_compute_trajectory(self):
        times = np.array([i for i in range(0, 3001, 600)] + [i for i in range(4001, 7001, 600)])

        self.assertEqual(6, compute_trajectory.py_func(times, 800))

    def test_compute_trajectory_empty(self):
        times = np.array([])

        self.assertEqual(0, compute_trajectory.py_func(times, 800))

    def test_apply_func_on_window(self):
        self.assertRaises(ValueError, apply_func_on_window, np.arange(10), 0, 0, 'not valid string')

    def test_apply_func_on_window_ignore(self):
        result = apply_func_on_window(np.arange(10), np.mean, 2, 'ignore')

        expected = np.array([1, 1.5, 2, 3, 4, 5, 6, 7, 7.5, 8])

        np.testing.assert_equal(result, expected)

    def test_apply_func_on_window_ignore_short(self):
        result = apply_func_on_window(np.arange(5), np.mean, 10, 'ignore')

        expected = np.array([2, 2, 2, 2, 2])

        np.testing.assert_equal(result, expected)

    def test_get_time_per_label_shift_single_label(self):
        trajectory = AISTrajectory(
            pd.DataFrame(
                {
                    "label": [1 for _ in range(0, 101, 10)],
                    "ts_sec": [i for i in range(0, 6001, 600)]
                }
            )
        )

        result = trajectory.get_time_per_label_shift()
        expected = [(0, 1)]

        self.assertListEqual(result, expected)

    def test_get_time_per_label_shift_label_switch_1(self):
        trajectory = AISTrajectory(
            pd.DataFrame(
                {
                    "label": [1 for _ in range(11)] + [2 for _ in range(10)],
                    "ts_sec": [i for i in range(0, 12001, 600)]
                }
            )
        )

        result = trajectory.get_time_per_label_shift()
        expected = [(0, 1), (6600, 2)]

        self.assertListEqual(result, expected)

    def test_get_time_per_label_shift_label_switch_2(self):
        trajectory = AISTrajectory(
            pd.DataFrame(
                {
                    "label": [1 for _ in range(11)] + [2 for _ in range(10)] + [1 for _ in range(10)],
                    "ts_sec": [i for i in range(0, 18001, 600)]
                }
            )
        )

        result = trajectory.get_time_per_label_shift()
        expected = [(0, 1), (6600, 2), (12600, 1)]

        self.assertListEqual(result, expected)

    def test_generate_array_from_positions(self):
        trajectory = AISTrajectory(
            pd.DataFrame(
                {
                    "latitude": [0, 10, 0, -10],
                    "longitude": [0, 10, 10, -10],
                    "ts_sec": [i for i in range(4)]
                }
            )
        )

        result = trajectory.generate_array_from_positions(height=9, width=9, link=False, bounding_box='fit',
                                                          features=None, node_size=0).reshape((9, 9))
        expected = np.array([[0, 0, 0, 0, 0, 0, 0, 0, 1],
                             [0, 0, 0, 0, 0, 0, 0, 0, 0],
                             [0, 0, 0, 0, 0, 0, 0, 0, 0],
                             [0, 0, 0, 0, 0, 0, 0, 0, 0],
                             [0, 0, 0, 0, 1, 0, 0, 0, 1],
                             [0, 0, 0, 0, 0, 0, 0, 0, 0],
                             [0, 0, 0, 0, 0, 0, 0, 0, 0],
                             [0, 0, 0, 0, 0, 0, 0, 0, 0],
                             [1, 0, 0, 0, 0, 0, 0, 0, 0]])

        np.testing.assert_array_equal(result, expected)

    def test_generate_array_from_positions_node_size(self):
        trajectory = AISTrajectory(
            pd.DataFrame(
                {
                    "latitude": [0, 10, 0, -10],
                    "longitude": [0, 10, 10, -10],
                    "ts_sec": [i for i in range(4)]
                }
            )
        )

        result = trajectory.generate_array_from_positions(height=9, width=9, link=False, bounding_box='fit',
                                                          features=None, node_size=1).reshape((9, 9))
        expected = np.array([[0, 0, 0, 0, 0, 0, 0, 1, 1],
                             [0, 0, 0, 0, 0, 0, 0, 1, 1],
                             [0, 0, 0, 0, 0, 0, 0, 0, 0],
                             [0, 0, 0, 1, 1, 1, 0, 1, 1],
                             [0, 0, 0, 1, 1, 1, 0, 1, 1],
                             [0, 0, 0, 1, 1, 1, 0, 1, 1],
                             [0, 0, 0, 0, 0, 0, 0, 0, 0],
                             [1, 1, 0, 0, 0, 0, 0, 0, 0],
                             [1, 1, 0, 0, 0, 0, 0, 0, 0]])

        np.testing.assert_array_equal(result, expected)

    def test_generate_array_from_positions_with_line(self):
        trajectory = AISTrajectory(
            pd.DataFrame(
                {
                    "latitude": [0, 10, 0, 20],
                    "longitude": [0, 10, 20, 20],
                    "ts_sec": [i for i in range(4)]
                }
            )
        )

        result = trajectory.generate_array_from_positions(height=9, width=18, link=True, bounding_box='fit',
                                                          features=None, node_size=0).reshape((9, 18))
        expected = np.array([[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1],
                             [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1],
                             [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1],
                             [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1],
                             [0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 1],
                             [0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 1],
                             [0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 1],
                             [0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 1],
                             [1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1]])

        np.testing.assert_array_equal(result, expected)

    def test_generate_array_from_positions_single_point(self):
        trajectory = AISTrajectory(
            pd.DataFrame(
                {
                    "latitude": [5],
                    "longitude": [20],
                    "ts_sec": [0]
                }
            )
        )

        result = trajectory.generate_array_from_positions(height=9, width=9, link=False, bounding_box='fit',
                                                          features=None).reshape((9, 9))
        expected = np.array([[0, 0, 0, 0, 0, 0, 0, 0, 0],
                             [0, 0, 0, 0, 0, 0, 0, 0, 0],
                             [0, 0, 0, 0, 0, 0, 0, 0, 0],
                             [0, 0, 0, 0, 0, 0, 0, 0, 0],
                             [0, 0, 0, 0, 1, 0, 0, 0, 0],
                             [0, 0, 0, 0, 0, 0, 0, 0, 0],
                             [0, 0, 0, 0, 0, 0, 0, 0, 0],
                             [0, 0, 0, 0, 0, 0, 0, 0, 0],
                             [0, 0, 0, 0, 0, 0, 0, 0, 0]])

        np.testing.assert_array_equal(result, expected)

    def test_generate_array_from_positions_overlapping_points(self):
        trajectory = AISTrajectory(
            pd.DataFrame(
                {
                    "latitude": [5, 5, 5, 5],
                    "longitude": [20, 20, 20, 20],
                    "ts_sec": [0, 1, 2, 3]
                }
            )
        )

        result = trajectory.generate_array_from_positions(height=9, width=9, link=False, bounding_box='fit',
                                                          features=None).reshape((9, 9))
        expected = np.array([[0, 0, 0, 0, 0, 0, 0, 0, 0],
                             [0, 0, 0, 0, 0, 0, 0, 0, 0],
                             [0, 0, 0, 0, 0, 0, 0, 0, 0],
                             [0, 0, 0, 0, 0, 0, 0, 0, 0],
                             [0, 0, 0, 0, 1, 0, 0, 0, 0],
                             [0, 0, 0, 0, 0, 0, 0, 0, 0],
                             [0, 0, 0, 0, 0, 0, 0, 0, 0],
                             [0, 0, 0, 0, 0, 0, 0, 0, 0],
                             [0, 0, 0, 0, 0, 0, 0, 0, 0]])

        np.testing.assert_array_equal(result, expected)
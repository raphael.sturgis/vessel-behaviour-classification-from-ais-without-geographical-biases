import unittest

import numpy as np
import pandas as pd

from skais.ais.ais_points import AISPoints


class TestAISPositions(unittest.TestCase):

    # Lazy test
    def test_init(self):
        ais_points = AISPoints(
            pd.DataFrame(
                {
                    "sog": [2, 3, 7, 15, 14, 12, 18, 25, 21, 12, 11, 16, 19, 2, 5, 15, 12, 7, 8, 9, 1],
                    "diff": [35, 45, 59, 12, 1, 2, 54, 5, 47, 86, 119, 68, 75, 54, 55, 12, 32, 62, 159, 157, 132],
                    "label": [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1],
                    "ts": [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1],
                    "mmsi": [0 for _ in range(21)]
                }
            )
        )

        self.assertIsNotNone(ais_points)

    def test_describe(self):
        ais_points = AISPoints(pd.DataFrame(
            {
                "sog": [2, 3, 7, 15, 14, 12, 18, 25, 21, 12, 11, 16, 19, 2, 5, 15, 12, 7, 8, 9, 1],
                "diff": [35, 45, 59, 12, 1, 2, 54, 5, 47, 86, 119, 68, 75, 54, 55, 12, 32, 62, 159, 157, 132],
                "label": [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1],
                "ts": [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1],
                "mmsi": [0 for _ in range(21)]
            }
        ))

        self.assertDictEqual(ais_points.describe(),
                             {
                                 'nb vessels': 1,
                                 'nb points': 21,
                                 'labeled 0': 13,
                                 'labeled 1': 8,
                                 'average speed': 234 / 21,
                                 'average diff': 1271 / 21
                             })

    def test_remove_outliers_simple(self):
        ais_points = AISPoints(pd.DataFrame(
            {
                "cog": [i for i in range(0, 359, 10)] + [1000] + [666],
                "heading": [0.0 for _ in range(0, 359, 10)] + [0] + [0]}
        )
        )
        expected = pd.DataFrame(
            {
                "cog": [i for i in range(0, 359, 10)] + [666],
                "heading": [0.0 for _ in range(0, 359, 10)] + [0]
            }
        )
        ais_points.remove_outliers(["cog", "heading"])
        pd.testing.assert_frame_equal(expected.reset_index(drop=True), ais_points.df.reset_index(drop=True))

    def test_remove_outliers_rank(self):
        ais_points = AISPoints(pd.DataFrame(
            {
                "cog": [i for i in range(0, 359, 10)] + [1000] + [666],
                "heading": [0.0 for _ in range(0, 359, 10)] + [0] + [0]}
        )
        )
        expected = pd.DataFrame(
            {
                "cog": [i for i in range(0, 359, 10)],
                "heading": [0.0 for _ in range(0, 359, 10)]
            }
        )
        ais_points.remove_outliers(["cog", "heading"], rank=2)
        pd.testing.assert_frame_equal(expected.reset_index(drop=True), ais_points.df.reset_index(drop=True))

    def test_remove_outliers_not_all_features(self):
        ais_points = AISPoints(pd.DataFrame(
            {
                "cog": [i / 350.0 for i in range(0, 359, 10)] + [500] + [0],
                "heading": [0.0 for _ in range(0, 359, 10)] + [0] + [10000]}
        )
        )
        expected = pd.DataFrame(
            {
                "cog": [i / 350.0 for i in range(0, 359, 10)] + [0],
                "heading": [0.0 for _ in range(0, 359, 10)] + [10000]
            }
        )
        ais_points.remove_outliers(["cog"])
        pd.testing.assert_frame_equal(expected.reset_index(drop=True), ais_points.df.reset_index(drop=True))

    def test_remove_outliers_exception(self):
        ais_points = AISPoints(
            pd.DataFrame(
                {
                    "cog": [i / 350.0 for i in range(0, 359, 10)] + [500] + [0],
                    "heading": [0.0 for _ in range(0, 359, 10)] + [0] + [10000]
                }
            )
        )
        with self.assertRaises(ValueError):
            ais_points.remove_outliers(["cog"], rank=0)

    def test_clean_angles(self):
        ais_points = AISPoints(pd.DataFrame(
            {
                "cog": [i for i in range(0, 359, 10)] + [489, 456, -12] + [180, 180, 180],
                "heading": [180 for _ in range(0, 359, 10)] + [489, 180, 180] + [999, 666, -333],
            }
        )
        )

        expected = pd.DataFrame(
            {
                "cog": [i for i in range(0, 359, 10)],
                "heading": [180 for _ in range(0, 359, 10)]
            }
        )

        ais_points.clean_angles()
        result = ais_points.df

        pd.testing.assert_frame_equal(expected.reset_index(drop=True), result.reset_index(drop=True))

    def test_normalize_min_max(self):
        ais_points = AISPoints(pd.DataFrame(
            {
                "cog": [i for i in range(0, 359, 10)],
                "heading": [180.0 for _ in range(0, 359, 10)]
            }
        )
        )

        ais_points.normalize(min_max_features=["cog", "heading"])
        result = ais_points.df
        expected = pd.DataFrame(
            {
                "cog": [i / 350.0 for i in range(0, 359, 10)],
                "heading": [0.0 for _ in range(0, 359, 10)]
            }
        )

        pd.testing.assert_frame_equal(expected.reset_index(drop=True), result.reset_index(drop=True))

    def test_normalize_standardization(self):
        ais_points = AISPoints(pd.DataFrame(
            {
                "cog": [i for i in range(0, 359, 10)],
                "heading": [180 for _ in range(0, 359, 10)]
            }
        )
        )

        ais_points.normalize(standardization_features=['cog', 'heading'])
        result = ais_points.df
        expected = pd.DataFrame(
            {
                "cog": [-1.68458833, -1.58832614, -1.49206395, -1.39580176, -1.29953957,
                        -1.20327738, -1.10701519, -1.010753, -0.91449081, -0.81822862,
                        -0.72196643, -0.62570424, -0.52944205, -0.43317986, -0.33691767,
                        -0.24065548, -0.14439329, -0.0481311, 0.0481311, 0.14439329,
                        0.24065548, 0.33691767, 0.43317986, 0.52944205, 0.62570424,
                        0.72196643, 0.81822862, 0.91449081, 1.010753, 1.10701519,
                        1.20327738, 1.29953957, 1.39580176, 1.49206395, 1.58832614,
                        1.68458833],
                "heading": [0.0 for _ in range(0, 359, 10)]
            }
        )

        pd.testing.assert_frame_equal(expected.reset_index(drop=True), result.reset_index(drop=True),
                                      check_exact=False, rtol=0.05)

    def test_normalize_3r_quartile(self):
        ais_points = AISPoints(pd.DataFrame(
            {
                "cog": [i for i in range(0, 359, 10)],
                "heading": [180 for _ in range(0, 359, 10)]
            }
        )
        )

        ais_points.normalize(third_quartile_features=["cog", "heading"])
        result = ais_points.df
        expected = pd.DataFrame(
            {
                "cog": [i / 270.0 for i in range(0, 359, 10)],
                "heading": [1.0 for _ in range(0, 359, 10)]
            }
        )

        pd.testing.assert_frame_equal(expected.reset_index(drop=True), result.reset_index(drop=True),
                                      check_exact=False, rtol=0.05)

    def test_normalize_divide_by_value(self):
        ais_points = AISPoints(pd.DataFrame(
            {
                "cog": [i for i in range(0, 359, 10)],
                "heading": [180 for _ in range(0, 359, 10)]
            }
        )
        )

        ais_points.normalize(divide_by_value=[("cog", 10), ("heading", 18)])
        result = ais_points.df
        expected = pd.DataFrame(
            {
                "cog": [i / 10 for i in range(0, 359, 10)],
                "heading": [10.0 for _ in range(0, 359, 10)]
            }
        )

        pd.testing.assert_frame_equal(expected.reset_index(drop=True), result.reset_index(drop=True),
                                      check_exact=False, rtol=0.05)

    def test_compute_drift(self):
        ais_points = AISPoints(pd.DataFrame(
            {
                "cog": [i for i in range(0, 359, 10)],
                "heading": [180 for _ in range(0, 359, 10)]
            }
        )
        )

        ais_points.compute_drift()

        diff = ais_points.df['drift'].to_numpy()

        np.testing.assert_array_equal(diff, np.array([180, 170, 160, 150, 140, 130, 120, 110, 100, 90, 80, 70, 60, 50,
                                                      40, 30, 20, 10, 0, 10, 20, 30, 40, 50, 60, 70, 80, 90, 100, 110,
                                                      120, 130, 140, 150, 160, 170]))

    def test_load_from_csv(self):
        result = AISPoints.load_from_csv("skais/tests/ais/test_load_from_csv.csv")

        ais_points = AISPoints(pd.DataFrame(
            {
                "sog": [2, 3, 7, 15, 14, 12, 18, 25, 21, 12, 11, 16, 19, 2, 5, 15, 12, 7, 8, 9, 1],
                "diff": [35, 45, 59, 12, 1, 2, 54, 5, 47, 86, 119, 68, 75, 54, 55, 12, 32, 62, 159, 157, 132],
                "label": [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1],
                "ts": [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1],
                "mmsi": [0 for _ in range(21)]
            }
        ))

        pd.testing.assert_frame_equal(result.df.reset_index(drop=True), ais_points.df.reset_index(drop=True))

    def test_fuse_single(self):
        ais_points = AISPoints(pd.DataFrame(
            {
                "sog": [2, 3, 7, 15, 14, 12, 18, 25, 21, 12, 11, 16, 19, 2, 5, 15, 12, 7, 8, 9, 1],
                "diff": [35, 45, 59, 12, 1, 2, 54, 5, 47, 86, 119, 68, 75, 54, 55, 12, 32, 62, 159, 157, 132],
                "label": [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1],
                "ts": [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1],
                "mmsi": [0 for _ in range(21)]
            }
        ))

        pd.testing.assert_frame_equal(AISPoints.fuse(ais_points).df, ais_points.df)

    def test_fuse_simple_list(self):
        ais_points = AISPoints(pd.DataFrame(
            {
                "sog": [2, 3, 7, 15, 14, 12, 18, 25, 21, 12, 11, 16, 19, 2, 5, 15, 12, 7, 8, 9, 1],
                "diff": [35, 45, 59, 12, 1, 2, 54, 5, 47, 86, 119, 68, 75, 54, 55, 12, 32, 62, 159, 157, 132],
                "label": [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1],
                "ts": [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1],
                "mmsi": [0 for _ in range(21)]
            }
        ))

        pd.testing.assert_frame_equal(AISPoints.fuse([ais_points]).df, ais_points.df)

    def test_fuse_multiple(self):
        ais_points = AISPoints(pd.DataFrame(
            {
                "sog": [2, 3, 7, 15, 14, 12, 18, 25, 21, 12, 11, 16, 19, 2, 5, 15, 12, 7, 8, 9, 1],
                "diff": [35, 45, 59, 12, 1, 2, 54, 5, 47, 86, 119, 68, 75, 54, 55, 12, 32, 62, 159, 157, 132],
                "label": [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1],
                "ts": [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                "mmsi": [0 for _ in range(21)]
            }
        ))

        value = pd.DataFrame(
            {
                "sog": [2, 3, 7, 15, 14, 12, 18, 25, 21, 12, 11, 16, 19, 2, 5, 15, 12, 7, 8, 9, 1, 2, 3, 7, 15, 14, 12,
                        18, 25, 21, 12, 11, 16, 19, 2, 5, 15, 12, 7, 8, 9, 1],
                "diff": [35, 45, 59, 12, 1, 2, 54, 5, 47, 86, 119, 68, 75, 54, 55, 12, 32, 62, 159, 157, 132, 35, 45,
                         59, 12, 1, 2, 54, 5, 47, 86, 119, 68, 75, 54, 55, 12, 32, 62, 159, 157, 132],
                "label": [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                          0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1],
                "ts": [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                       0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                "mmsi": [0 for _ in range(42)]
            }
        )

        value['ts'] = pd.to_datetime(value.ts)
        pd.testing.assert_frame_equal(AISPoints.fuse(ais_points, ais_points).df.reset_index(drop=True),
                                      value.reset_index(drop=True))

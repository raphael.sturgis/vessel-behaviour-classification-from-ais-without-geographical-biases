import unittest
import numpy as np

from skais.utils.geography import position_from_distance

tol = 0.005


class TestGeography(unittest.TestCase):
    def test_position_from_distance_1(self):
        position = (0, 0)
        distance = (10000, 10000)

        expected = (0.09, 0.09)
        result = position_from_distance(position, distance)

        np.testing.assert_allclose(expected, result, tol)

    def test_position_from_distance_2(self):
        position = (10, 10)
        distance = (10000, 10000)

        expected = (10.0636111, 10.064722222222223)
        result = position_from_distance(position, distance)

        np.testing.assert_allclose(expected, result, tol)

    def test_position_from_distance_3(self):
        position = (75, 75)
        distance = (10000, -10000)

        expected = (75.0633333, 74.75333333333333)
        result = position_from_distance(position, distance)

        np.testing.assert_allclose(expected, result, tol)


if __name__ == '__main__':
    unittest.main()

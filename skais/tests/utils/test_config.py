# -*- coding: utf-8 -*-
"""

.. moduleauthor:: Valentin Emiya
"""
from configparser import ConfigParser
import os
from pathlib import Path
import tempfile
import unittest
from unittest.mock import patch

from skais.utils.config import generate_config, get_data_path, get_config_file


class TestGetConfigFile(unittest.TestCase):
    def test_get_config_file(self):
        config_file = get_config_file()
        self.assertEqual(config_file.name, 'skais.conf')
        self.assertEqual(config_file.parent.name,  '.config')
        self.assertEqual(config_file.parent.parent,
                         Path(os.path.expanduser('~')))


class TestGenerateConfig(unittest.TestCase):
    def test_generate_config(self):
        with patch('skais.utils.config.get_config_file') as mock:
            mock.return_value = Path(tempfile.mkdtemp()) / 'skais.conf'
            config_file = mock.return_value
            self.assertFalse(config_file.exists())
            generate_config()
            self.assertTrue(config_file.exists())


class TestGetDataPath(unittest.TestCase):
    def test_get_data_path(self):
        with patch('skais.utils.config.get_config_file') as mock:
            mock.return_value = Path(tempfile.mkdtemp()) / 'skais.conf'
            config_file = mock.return_value

            self.assertFalse(config_file.exists())
            with self.assertRaises(Exception):
                get_data_path()

            generate_config()
            with self.assertRaises(Exception):
                get_data_path()

            config = ConfigParser()
            config.read(config_file)
            true_data_path = Path(tempfile.mkdtemp()) / 'data'
            true_data_path.mkdir()
            print(true_data_path)
            self.assertTrue(true_data_path.exists())
            print('Data path:', str(true_data_path))
            config.set('DATA', 'data_path', str(true_data_path))
            config.write(open(config_file, 'w'))
            tested_data_path = get_data_path()
            self.assertEqual(tested_data_path, true_data_path)

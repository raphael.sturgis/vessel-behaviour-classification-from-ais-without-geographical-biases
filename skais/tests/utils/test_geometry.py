import unittest

from skais.utils.geometry import bresenham


class TestGeometry(unittest.TestCase):
    def test_bresenham(self):
        result = bresenham(3, 4, 16, 9)
        expected = [(3, 4), (4, 4), (5, 5), (6, 5), (7, 6), (8, 6), (9, 6), (10, 7), (11, 7), (12, 7), (13, 8), (14, 8),
                    (15, 9), (16, 9)]

        self.assertListEqual(result, expected)

    def test_bresenham_inverted(self):
        result = bresenham(16, 9, 3, 4)
        expected = [(3, 4), (4, 4), (5, 5), (6, 5), (7, 6), (8, 6), (9, 6), (10, 7), (11, 7), (12, 7), (13, 8),
                    (14, 8), (15, 9), (16, 9)]

        self.assertListEqual(result, expected)


    def test_bresenham_inverted_2(self):
        result = bresenham(16, 4, 3, 9)
        expected = [(3, 9), (4, 9), (5, 8), (6, 8), (7, 7), (8, 7), (9, 7), (10, 6), (11, 6), (12, 6), (13, 5), (14, 5),
                    (15, 4), (16, 4)]
        self.assertListEqual(result, expected)

    def test_bresenham_same_line(self):
        result = bresenham(3, 4, 10, 4)
        expected = [(3, 4), (4, 4), (5, 4), (6, 4), (7, 4), (8, 4), (9, 4), (10, 4)]

        self.assertListEqual(result, expected)

    def test_bresenham_same_column(self):
        result = bresenham(3, 4, 3, 10)
        expected = [(3, 4), (3, 5), (3, 6), (3, 7), (3, 8), (3, 9), (3, 10)]

        self.assertListEqual(result, expected)


if __name__ == '__main__':
    unittest.main()

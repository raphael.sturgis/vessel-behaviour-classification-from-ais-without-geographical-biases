import unittest

import numpy as np

from skais.process.basic_features_operations import angular_mean, angular_std, angular_time_derivative


class TestAngular(unittest.TestCase):
    def test_angular_mean_simple(self):
        x = np.radians(np.array([1, 359]))

        self.assertEqual(2*np.pi, angular_mean(x))

    def test_angular_mean_simple_2(self):
        x = np.radians(np.array([180, 180, 180, 180, 179, 181]))

        self.assertEqual(np.pi, angular_mean(x))

    def test_angular_mean_simple_3(self):
        x = np.radians(np.array([0, 0, 0, 0, 359, 1]))

        self.assertEqual(2*np.pi, angular_mean(x))

    def test_angular_mean_first_quadrant(self):
        x = np.radians(np.array([43, 44, 45, 46, 47]))

        self.assertEqual(np.pi / 4, angular_mean(x))

    def test_angular_mean_second_quadrant(self):
        x = np.radians(np.array([133, 134, 135, 136, 137]))

        self.assertEqual(3 * np.pi / 4, angular_mean(x))

    def test_angular_mean_third_quadrant(self):
        x = np.radians(np.array([223, 224, 225, 226, 227]))

        self.assertEqual(5 * np.pi / 4, angular_mean(x))

    def test_angular_mean_fourth_quadrant(self):
        x = np.radians(np.array([313, 314, 315, 316, 317]))

        self.assertEqual(7*np.pi / 4, angular_mean(x))

    def test_angular_std(self):
        x = np.radians(np.array([0, 0, 0, 0]))

        self.assertEqual(0, angular_std(x))

    def test_angular_std_2(self):
        x = np.radians(np.array([0, 90, 180, 270]))

        self.assertAlmostEqual(1, angular_std(x))

    def test_angular_time_derivative(self):
        result = angular_time_derivative(0, np.pi, 0, 1)

        self.assertAlmostEqual(np.pi, result)

    def test_angular_time_derivative_2(self):
        result = angular_time_derivative(7*np.pi/4, np.pi/4, 0, 1)

        self.assertAlmostEqual(np.pi/2, result)
import unittest
import pandas as pd

from skais.ais.ais_trajectory import AISTrajectory
from skais.process.data_augmentation.flip import Flip


class TestFlip(unittest.TestCase):
    def setUp(self):
        t1 = AISTrajectory(
            pd.DataFrame(
                {
                    'ts_sec': [i for i in range(10)],
                    'latitude': [45 + i for i in range(10)],
                    'longitude': [12 + i for i in range(10)]
                }
            )
        )
        t2 = AISTrajectory(
            pd.DataFrame(
                {
                    'ts_sec': [i for i in range(10)],
                    'latitude': [-12 + i for i in range(10)],
                    'longitude': [12 + i for i in range(10)]
                }
            )
        )

        self.trajectories = [t1, t2]

    def test_flip_equator(self):
        aug = Flip(None, 0)

        result = aug.transform(self.trajectories)

        t1 = AISTrajectory(
            pd.DataFrame(
                {
                    'ts_sec': [i for i in range(10)],
                    'latitude': [-45 - i for i in range(10)],
                    'longitude': [12 + i for i in range(10)]
                }
            )
        )
        t2 = AISTrajectory(
            pd.DataFrame(
                {
                    'ts_sec': [i for i in range(10)],
                    'latitude': [12 - i for i in range(10)],
                    'longitude': [12 + i for i in range(10)]
                }
            )
        )
        expected = [t1, t2]

        self.assertEqual(len(expected), len(result))
        for t1, t2 in zip(result, expected):
            pd.testing.assert_frame_equal(t1.df, t2.df)

    def test_invariance(self):
        t1 = AISTrajectory(
            pd.DataFrame(
                {
                    'ts_sec': [i for i in range(10)],
                    'latitude': [45 + i for i in range(10)],
                    'longitude': [12 + i for i in range(10)]
                }
            )
        )
        t2 = AISTrajectory(
            pd.DataFrame(
                {
                    'ts_sec': [i for i in range(10)],
                    'latitude': [-12 + i for i in range(10)],
                    'longitude': [12 + i for i in range(10)]
                }
            )
        )
        expected = [t1, t2]
        aug = Flip(0, None)

        _ = aug.transform(self.trajectories)
        for t1, t2 in zip(self.trajectories, expected):
            pd.testing.assert_frame_equal(t1.df, t2.df)


if __name__ == '__main__':
    unittest.main()

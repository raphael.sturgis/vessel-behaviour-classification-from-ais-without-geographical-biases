import unittest
from skais.process.geography import *


class MyTestCase(unittest.TestCase):
    def test_to_rad_0(self):
        result = to_rad.py_func(0)
        expected = 0

        self.assertAlmostEqual(result, expected)

    def test_to_rad_90(self):
        result = to_rad.py_func(90)
        expected = np.pi / 2

        self.assertAlmostEqual(result, expected)

    def test_to_rad_180(self):
        result = to_rad.py_func(180)
        expected = np.pi

        self.assertAlmostEqual(result, expected)

    def test_to_rad_360(self):
        result = to_rad.py_func(360)
        expected = 2 * np.pi

        self.assertAlmostEqual(result, expected)

    def test_to_rad_m180(self):
        result = to_rad.py_func(-180)
        expected = -np.pi

        self.assertAlmostEqual(result, expected)

    def test_to_rad_m90(self):
        result = to_rad.py_func(-90)
        expected = -np.pi / 2

        self.assertAlmostEqual(result, expected)

    def test_to_rad_270(self):
        result = to_rad.py_func(270)
        expected = 3 * np.pi / 2

        self.assertAlmostEqual(result, expected)

    def test_to_rad_810(self):
        result = to_rad.py_func(810)
        expected = 9 * np.pi / 2

        self.assertAlmostEqual(result, expected)

    def test_to_deg_0(self):
        result = to_deg.py_func(0)
        expected = 0

        self.assertAlmostEqual(result, expected)

    def test_to_deg_90(self):
        result = to_deg.py_func(np.pi / 2)
        expected = 90

        self.assertAlmostEqual(result, expected)

    def test_to_deg_180(self):
        result = to_deg.py_func(np.pi)
        expected = 180

        self.assertAlmostEqual(result, expected)

    def test_to_deg_360(self):
        result = to_deg.py_func(2 * np.pi)
        expected = 360

        self.assertAlmostEqual(result, expected)

    def test_to_deg_m180(self):
        result = to_deg.py_func(-np.pi)
        expected = -180

        self.assertAlmostEqual(result, expected)

    def test_to_deg_m90(self):
        result = to_deg.py_func(-np.pi / 2)
        expected = -90

        self.assertAlmostEqual(result, expected)

    def test_to_deg_270(self):
        result = to_deg.py_func(3 * np.pi / 2)
        expected = 270

        self.assertAlmostEqual(result, expected)

    def test_to_deg_810(self):
        result = to_deg.py_func(9 * np.pi / 2)
        expected = 810

        self.assertAlmostEqual(result, expected)

    def test_bearing_rand(self):
        paris = (2.3522, 48.8566)
        marseille = (5.3698, 43.2965)

        result = bearing.py_func(paris, marseille)
        expected = 158.2694

        self.assertAlmostEqual(result, expected, places=4)

    def test_bearing_along_equator(self):
        p1 = (0, 0)
        p2 = (90, 0)

        result = bearing.py_func(p1, p2)
        expected = 90

        self.assertAlmostEqual(result, expected)

    def test_bearing_equator_to_north_pole(self):
        p1 = (0, 0)
        p2 = (0, 90)

        result = bearing.py_func(p1, p2)
        expected = 0

        self.assertAlmostEqual(result, expected)

    def test_angle_between_three_points_1(self):
        p1 = (0, -10)
        p2 = (0, 0)
        p3 = (10, 0)

        self.assertAlmostEqual(90, angle_between_three_points.py_func(p1, p2, p3), places=3)

    def test_angle_between_three_points_2(self):
        p1 = (0, -10)
        p2 = (0, 0)
        p3 = (-10, 0)

        self.assertAlmostEqual(-90, angle_between_three_points.py_func(p1, p2, p3), places=3)

    def test_angle_between_three_points_3(self):
        p1 = (0, -10)
        p2 = (0, 0)
        p3 = (10, 10)

        self.assertAlmostEqual(180 - 44.56139, angle_between_three_points.py_func(p1, p2, p3), places=3)

    def test_angle_between_three_points_4(self):
        p1 = (0, 0)
        p2 = (0, 10)
        p3 = (0, 0)

        self.assertAlmostEqual(0, abs(angle_between_three_points.py_func(p1, p2, p3)), places=3)

    def test_compute_point_angles(self):
        dat = np.array([(0, 0), (0, 10), (10, 10), (0, 10)])
        result = compute_point_angles(dat)

        expected = np.array([0, 90, 0, 0])

        np.testing.assert_almost_equal(expected, result, decimal=0)
        self.assertTrue(True)


if __name__ == '__main__':
    unittest.main()

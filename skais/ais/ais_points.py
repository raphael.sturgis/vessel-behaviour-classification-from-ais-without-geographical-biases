import numpy as np
import pandas as pd
from scipy.stats import stats


class AISPoints:

    # Todo: Should be more elegant
    def __init__(self, df=None):
        if 'ts' in df:
            df['ts'] = pd.to_datetime(df.ts)
        if 'mmsi' in df:
            df = df.sort_values(['mmsi', 'ts'])

        self.df = df

    def describe(self):
        description = {
            "nb vessels": len(self.df.mmsi.unique()),
            "nb points": len(self.df.index),
            "average speed": self.df['sog'].mean(),
            "average diff": self.df['diff'].mean()
        }

        for n in np.sort(self.df['label'].unique()):
            description[f"labeled {n}"] = len(self.df[self.df['label'] == n].index)

        return description

    # cleaning functions
    def remove_outliers(self, features, rank=4):
        if rank <= 0:
            raise ValueError(f"Rank is equal to {rank}, must be positive and superior to 0")
        for feature in features:
            self.df = self.df.drop(self.df[(np.abs(stats.zscore(self.df[feature])) > rank)].index)

    def clean_angles(self):
        self.df = self.df[self.df["cog"] <= 360]
        self.df = self.df[self.df["cog"] >= 0]

        self.df = self.df[self.df["heading"] <= 360]
        self.df = self.df[self.df["heading"] >= 0]

    def normalize(self, min_max_features=(), standardization_features=(), third_quartile_features=(),
                  divide_by_value=(), divide_by_max=(), normalization_dict=None):
        if normalization_dict is None:
            normalization_dict = {}
            for f in min_max_features:
                if f in self.df.columns:
                    normalization_dict[f] = {'type': 'min-max'}
                    minimum = self.df[f].min()
                    maximum = self.df[f].max()
                    diff = (maximum - minimum)
                    if diff == 0:
                        print("Warning: diff = 0")
                        self.df[f] = (self.df[f] - minimum)
                    else:
                        self.df[f] = (self.df[f] - minimum) / diff
                    normalization_dict[f]["minimum"] = minimum
                    normalization_dict[f]["maximum"] = maximum
            for f in standardization_features:
                if f in self.df.columns:
                    normalization_dict[f] = {'type': 'standardization'}
                    mean = self.df[f].mean()
                    std = self.df[f].std()
                    if std == 0:
                        print("Warning: std = %d", std)
                        std = 1
                    self.df[f] = (self.df[f] - mean) / std
                    normalization_dict[f]["mean"] = mean
                    normalization_dict[f]["std"] = std
            for f in third_quartile_features:
                if f in self.df.columns:
                    normalization_dict[f] = {'type': '3rd quartile'}
                    third_quartile = self.df[f].quantile(0.75)
                    if third_quartile == 0:
                        print("Warning: third quartile = %d", third_quartile)
                        third_quartile = 1
                    self.df[f] = self.df[f] / third_quartile
                    normalization_dict[f]["value"] = third_quartile
            for t in divide_by_value:
                f = t[0]
                value = t[1]
                if f in self.df.columns:
                    if value != 0:
                        normalization_dict[f] = {'type': 'divide by value',
                                                 'value': value}
                        self.df[f] = self.df[f] / value
                    else:
                        print("Warning: dividing by 0")
            for f in divide_by_max:
                if f in self.df.columns:
                    maximum = self.df[f].max()
                    normalization_dict[f] = {'type': 'divide by max',
                                             'maximum': maximum}
                    self.df[f] = self.df[f] / maximum
        else:
            for f in normalization_dict:
                if f in self.df.columns:
                    if normalization_dict[f]['type'] == 'min-max':
                        minimum = normalization_dict[f]["minimum"]
                        maximum = normalization_dict[f]["maximum"]
                        diff = (maximum - minimum)
                        if diff == 0:
                            print("Warning: diff = 0")
                            diff = 1
                        self.df[f] = (self.df[f] - minimum) / diff
                    elif normalization_dict[f]['type'] == "standardization":
                        mean = normalization_dict[f]["mean"]
                        std = normalization_dict[f]["std"]
                        if std == 0:
                            print("Warning: std = 0")
                            std = 1
                        self.df[f] = (self.df[f] - mean) / std
                    elif normalization_dict[f]['type'] == "3rd quartile":
                        third_quartile = normalization_dict[f]["value"]
                        self.df[f] = self.df[f] / third_quartile
                    elif normalization_dict[f]['type'] == "divide by value":
                        value = normalization_dict[f]["value"]
                        self.df[f] = self.df[f] / value
                    elif normalization_dict[f]['type'] == "divide by max":
                        maximum = normalization_dict[f]["maximum"]
                        self.df[f] = self.df[f] / maximum
                    else:
                        raise ValueError(
                            f"{normalization_dict[f]['type']} not a valid normalization method. Must be on of [min-max,"
                            f" standardization, 3rd quartile, divide by value]")
        return normalization_dict

    # New features
    def compute_drift(self):
        self.df["drift"] = self.df.apply(lambda x: 180 - abs(abs(x['heading'] - x['cog']) - 180),
                                         axis=1)

    # Static methods
    @staticmethod
    def fuse(*args):
        if len(args) == 1:
            if not isinstance(args[0], list):
                return args[0]
            else:
                table = args[0]
        else:
            table = args
        dfs = []
        for aisPosition in table:
            dfs.append(aisPosition.df)

        return AISPoints(pd.concat(dfs).reindex())

    @staticmethod
    def load_from_csv(file_name):
        df = pd.read_csv(file_name)
        ais_points = AISPoints(df)
        return ais_points

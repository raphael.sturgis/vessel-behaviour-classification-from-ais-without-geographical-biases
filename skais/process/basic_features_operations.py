import cmath

import numpy as np


def angular_average_vector(angles):
    n = len(angles)
    x = np.sum(np.cos(angles)) / n
    y = np.sum(np.sin(angles)) / n

    return np.array([x, y])


def angular_dispersion(angles):
    x, y = angular_average_vector(angles)
    return np.sqrt(x ** 2 + y ** 2)


def angular_mean(angles):
    x, y = angular_average_vector(angles)
    theta = np.arctan(y/x)

    if y > 0 and x > 0:
        return theta
    elif x <= 0:
        return np.pi + theta
    else:
        return 2*np.pi + theta


def angular_std(angles):
    return 1 - angular_dispersion(angles)


def angular_time_derivative(angle1, angle2, ts1, ts2):
    z1 = np.cos(angle1) + np.sin(angle1) * 1j
    z2 = np.cos(angle2) + np.sin(angle2) * 1j

    z = z2 / z1
    return cmath.polar(z)[1] / (ts2 - ts1)


def time_derivative(f1, f2, ts1, ts2):
    return (f2 - f1) / (ts2 - ts1)

import tqdm as tqdm

from skais.process.data_augmentation.data_transformer import DataTransformer
from skais.process.data_augmentation.flip import Flip
from skais.process.data_augmentation.pipeline import Pipeline
from skais.process.data_augmentation.translator import Translator


class AugmentationEngine:
    def __init__(self, translation_values=None, flip_values=None, keep_original=True):
        self.pipelines = []
        if keep_original:
            self.pipelines.append(DataTransformer())

        if translation_values is not None:
            for tv_long, tv_lat in translation_values:
                self.pipelines.append(Pipeline([Translator(tv_long, tv_lat)]))

        if flip_values is not None:
            for fv_meridian, fv_parallel in flip_values:
                self.pipelines.append(Pipeline([Flip(fv_meridian, fv_parallel)]))

        if flip_values is not None and translation_values is not None:
            for tv_long, tv_lat in translation_values:
                translator = Translator(tv_long, tv_lat)
                for fv_meridian, fv_parallel in flip_values:
                    flip = Flip(fv_meridian, fv_parallel)
                    self.pipelines.append(Pipeline([translator, flip]))

    def transform(self, x, verbose=0):
        results = []

        iterator = self.pipelines
        if verbose > 0:
            iterator = tqdm.tqdm(self.pipelines)
        for p in iterator:
            results += p.transform(x)

        return results

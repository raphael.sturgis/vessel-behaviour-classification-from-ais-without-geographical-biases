import pickle

import matplotlib.pyplot as plt
import numpy as np
from sklearn.metrics import accuracy_score
import tqdm

from skais.ais.ais_points import AISPoints
from skais.utils.config import get_data_path
from sklearn.model_selection import GridSearchCV, ParameterGrid
from sklearn.tree import DecisionTreeClassifier

from Experiment_tools.features import FEATURES_LIST

param_grid = {'criterion': ['gini', 'entropy'], 'splitter': ["best", "random"],
              'max_depth': [1, 2, 5, 10], "class_weight": ["balanced"]}

FEATURES_NAMES = ["SOG", "Drift", "$\Delta_{SOG}$", "$\Delta_{COG}$", "$\Delta_{Heading}$", "$std_{COG}$",
                  "$std_{Heading}$", "$std_{\Delta_{COG}}$", "$std_{\Delta_{Heading}}$", "$std_{Position}$",
                  "$std_{SOG}$"]


def get_model(param_grid, x_train, y_train, x_test, y_test, runs=5, verbose=1):
    best_acc = 0
    best_model = None
    for params in tqdm.tqdm(list(ParameterGrid(param_grid)), disable=verbose < 1):
        for _ in range(runs):
            dt = DecisionTreeClassifier(criterion=params['criterion'], splitter=params['splitter'],
                                        max_depth=params['max_depth'], class_weight=params['class_weight'])
            dt.fit(x_train, y_train)
            y_pred = dt.predict(x_test)
            acc = accuracy_score(y_test, y_pred)
            if acc > best_acc:
                best_acc = acc
                best_model = dt
    return best_model


if __name__ == '__main__':
    train = pickle.load(open(f"{get_data_path()}/experiments/ITSC/experiment2/train_eng.pkl", 'rb'))
    test = pickle.load(open(f"{get_data_path()}/experiments/ITSC/experiment2/test_eng.pkl", 'rb'))

    points_train = AISPoints.fuse(train)
    points_test = AISPoints.fuse(test)

    X_train = points_train.df[FEATURES_LIST].to_numpy()
    X_test = points_test.df[FEATURES_LIST].to_numpy()
    f = FEATURES_LIST

    y_train = points_train.df['label'].to_numpy()
    y_test = points_test.df['label'].to_numpy()

    model = get_model(param_grid, X_train, y_train, X_test, y_test, runs=5)

    zipped = list(zip(model.feature_importances_, FEATURES_NAMES))
    zipped.sort(reverse=True)
    importance, names = zip(*zipped)
    plt.figure(figsize=(12, 6))
    plt.bar(np.arange(len(FEATURES_LIST)),
            importance,
            width=0.5,
            label='Feature importance')
    plt.legend()
    plt.xticks(np.arange(len(FEATURES_LIST)), labels=names)
    plt.savefig(f'{get_data_path()}/plots/feature_importance.png')

import pickle
from pathlib import Path

import numpy as np
import tqdm
from skais.ais.ais_points import AISPoints
from skais.utils.config import get_data_path

from Experiment_tools.features import compute_selected_features
from Experiment_tools.data_import import get_western_med_trajectories

radius = 17
np.seterr(divide='ignore', invalid='ignore')

if __name__ == '__main__':
    Path(f"{get_data_path()}/experiments/ITSC/Experiment2/").mkdir(parents=True, exist_ok=True)
    Path(f"{get_data_path()}/experiments/ITSC/Experiment2/plots").mkdir(parents=True, exist_ok=True)
    
    mmsi_trajectories = get_western_med_trajectories()
    trajectories_split = []
    for trajectory in mmsi_trajectories:
        trajectories_split += trajectory.split_trajectory(time_gap=3600, interpolation=300)

    trajectories = []
    for trajectory in tqdm.tqdm(trajectories_split):
        if len(trajectory.df.index) > 12:
            trajectory.remove_outliers(['sog'])
            compute_selected_features(trajectory, radius)
            trajectories.append(trajectory)

    points = AISPoints.fuse(trajectories)
    pickle.dump(points, open(f'{get_data_path()}/experiments/ITSC/Experiment2/points.pkl', 'wb'))

    train_trajectories = pickle.load(open(f"{get_data_path()}/experiments/ITSC/data/wm_train.pkl", 'rb'))
    test_trajectories = pickle.load(open(f"{get_data_path()}/experiments/ITSC/data/wm_test.pkl", 'rb'))
    
    t_eng = []
    for trajectory in tqdm.tqdm(train_trajectories):
        compute_selected_features(trajectory, radius)
        t_eng.append(trajectory)

    pickle.dump(t_eng, open(f"{get_data_path()}/experiments/ITSC/experiment2/train_eng.pkl", 'wb'))

    t_eng = []
    for trajectory in tqdm.tqdm(test_trajectories):
        compute_selected_features(trajectory, radius)
        t_eng.append(trajectory)

    pickle.dump(t_eng, open(f"{get_data_path()}/experiments/ITSC/experiment2/test_eng.pkl", 'wb'))

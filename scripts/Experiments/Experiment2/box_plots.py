import pickle

import seaborn as sns
from skais.utils.config import get_data_path
import matplotlib.pyplot as plt
from pathlib import Path

from Experiment_tools.features import FEATURES_LIST

if __name__ == '__main__':
    points = pickle.load(open(f'{get_data_path()}/experiments/ITSC/Experiment2/points.pkl', 'rb'))
    Path(f"{get_data_path()}/plots/boxplot").mkdir(parents=True, exist_ok=True)
    for feature in FEATURES_LIST:
        sns.boxplot(x='label', y=feature, data=points.df)
        plt.savefig(f'{get_data_path()}/plots/boxplot/{feature}_boxplot.png')
        plt.clf()
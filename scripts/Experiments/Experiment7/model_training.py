import fcntl
import pickle
import sys

import numpy as np
from keras import Sequential
from keras.callbacks import EarlyStopping, ModelCheckpoint, ReduceLROnPlateau
from keras.layers import Bidirectional, LSTM, Dense, TimeDistributed
from keras.models import load_model
from sklearn import metrics

from Experiment_tools.experiment_tools import convert_to_vectors
from Experiment_tools.generator import MyBatchGenerator
from skais.utils.config import get_data_path

np.seterr(invalid='ignore')

NAME = "wm"
FEATURES_LIST = ['sog']

def create_model(nb_layers, nb_units, nb_inputs):
    model = Sequential()

    model.add(Bidirectional(LSTM(nb_units, return_sequences=True), input_shape=(None, nb_inputs)))
    for _ in range(nb_layers - 1):
        model.add(Bidirectional(LSTM(nb_units, return_sequences=True)))
    model.add(TimeDistributed(Dense(4, activation='softmax')))

    optimizer = "Adam"
    model.compile(loss='categorical_crossentropy',
                  optimizer=optimizer,
                  metrics=['accuracy'])

    return model


def run_model(model, x_train, y_train, x_test, y_test, name, get_history=False):
    early_stopping = EarlyStopping(monitor='val_loss', patience=10, mode='min')
    mcp_save = ModelCheckpoint(
        f"{get_data_path()}/experiments/ITSC/experiment7/models/lstm_{name}.hdf5"
        , save_best_only=True, monitor='val_loss',
        mode='min')
    reduce_lr_loss = ReduceLROnPlateau(monitor='val_loss', factor=0.1, patience=7, verbose=0, min_delta=1e-4,
                                       mode='min')
    history = model.fit(MyBatchGenerator(x_train, y_train, batch_size=1),
                        validation_data=MyBatchGenerator(x_test, y_test, batch_size=1),
                        callbacks=[early_stopping, mcp_save, reduce_lr_loss],
                        epochs=100)

    saved_model = load_model(
        f"{get_data_path()}/experiments/ITSC/experiment7/models/lstm_{name}.hdf5")

    if get_history:
        return history, saved_model
    return saved_model


def evaluate_model(model, x_test, y_test, run, nb_layers, nb_units):
    predictions = []
    for x in x_test:
        predictions.append(model.predict(np.array([x])))

    y_predict = np.concatenate(predictions, axis=1)
    y_true = np.concatenate(y_test)

    acc = metrics.accuracy_score(np.argmax(y_true, axis=1), np.argmax(y_predict[0], axis=1))
    precision = metrics.precision_score(np.argmax(y_true, axis=1), np.argmax(y_predict[0], axis=1), average='weighted')
    recall = metrics.recall_score(np.argmax(y_true, axis=1), np.argmax(y_predict[0], axis=1), average='weighted')
    f1 = metrics.f1_score(np.argmax(y_true, axis=1), np.argmax(y_predict[0], axis=1), average='weighted')

    with open(f"{get_data_path()}/experiments/ITSC/experiment7/results.csv", "a") as g:
        fcntl.flock(g, fcntl.LOCK_EX)
        g.write(f"{run},{nb_layers},{nb_units},{acc},{f1},{recall},{precision}\n")
        fcntl.flock(g, fcntl.LOCK_UN)


def run_lstm_instance(instance):
    run = int(instance[0])
    nb_layers = int(instance[1])
    nb_units = int(instance[2])

    train = pickle.load(
        open(
            f"{get_data_path()}/experiments/ITSC/experiment7/datasets/"
            f"{NAME}_eng_train.pkl", 'rb'))

    test = pickle.load(
        open(
            f"{get_data_path()}/experiments/ITSC/experiment7/datasets/"
            f"{NAME}_eng_test.pkl", 'rb'))

    x_train, y_train = convert_to_vectors(train, FEATURES_LIST)
    x_test, y_test = convert_to_vectors(test, FEATURES_LIST)

    model = create_model(nb_layers, nb_units, len(FEATURES_LIST))

    name = f"training_{run}_{nb_layers}_{nb_units}"
    model = run_model(model, x_train, y_train, x_test, y_test, name)

    evaluate_model(model, x_test, y_test, run, nb_layers, nb_units)


if __name__ == '__main__':
    f = open(f"{get_data_path()}/experiments/ITSC/experiment7/instances/{sys.argv[1]}.inst")
    lines = f.readlines()
    for line in lines:
        instance = line.split()
        run_lstm_instance(instance)
    print("success!")

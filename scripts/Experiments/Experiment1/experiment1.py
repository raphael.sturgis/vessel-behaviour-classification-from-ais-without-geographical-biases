import pickle
from pathlib import Path

import matplotlib.pyplot as plt
import numpy as np
import tqdm
from sklearn import metrics
from sklearn.metrics import accuracy_score
from sklearn.model_selection import ParameterGrid
from sklearn.tree import DecisionTreeClassifier

from Experiment_tools.features import FEATURES_LIST, compute_selected_features_on_trajectories
from skais.ais.ais_points import AISPoints
from skais.utils.config import get_data_path

np.seterr(invalid='ignore')

param_grid = {'criterion': ['gini', 'entropy'], 'splitter': ["best", "random"],
              'max_depth': [1, 2, 5, 10], "class_weight": ["balanced"]}


def get_model(param_grid, x_train, y_train, x_test, y_test, runs=5, verbose=1):
    best_acc = 0
    best_model = None
    for params in tqdm.tqdm(list(ParameterGrid(param_grid)), disable=verbose < 1):
        for _ in range(runs):
            dt = DecisionTreeClassifier(criterion=params['criterion'], splitter=params['splitter'],
                                        max_depth=params['max_depth'], class_weight=params['class_weight'])
            dt.fit(x_train, y_train)
            y_pred = dt.predict(x_test)
            acc = accuracy_score(y_test, y_pred)
            if acc > best_acc:
                best_acc = acc
                best_model = dt
    return best_model


if __name__ == '__main__':
    Path(f"{get_data_path()}/experiments/ITSC/experiment1/").mkdir(parents=True, exist_ok=True)

    train_trajectories = pickle.load(open(f"{get_data_path()}/experiments/ITSC/data/wm_train.pkl", 'rb'))
    test_trajectories = pickle.load(open(f"{get_data_path()}/experiments/ITSC/data/wm_test.pkl", 'rb'))
    val_trajectories = pickle.load(open(f"{get_data_path()}/experiments/ITSC/data/wm_val.pkl", 'rb'))

    performances_acc = []
    performances_f1 = []
    performances_acc_std = []
    performances_f1_std = []

    trajectories_count = []
    values = []

    max_radius = 80

    done = False  # TODO: remove
    for radius in tqdm.tqdm(range(1, max_radius, 40)):
        values.append(radius)

        trajectories_train = compute_selected_features_on_trajectories(train_trajectories, radius=radius)
        trajectories_test = compute_selected_features_on_trajectories(test_trajectories, radius=radius)
        trajectories_val = compute_selected_features_on_trajectories(val_trajectories, radius=radius)

        points_train = AISPoints.fuse(trajectories_train)
        points_test = AISPoints.fuse(trajectories_test)
        points_val = AISPoints.fuse(trajectories_val)

        x_train = points_train.df[FEATURES_LIST].to_numpy()
        y_train = points_train.df['label'].to_numpy()

        x_test = points_test.df[FEATURES_LIST].to_numpy()
        y_test = points_test.df['label'].to_numpy()

        x_val = points_val.df[FEATURES_LIST].to_numpy()
        y_val = points_val.df['label'].to_numpy()

        accs = []
        f1s = []
        for _ in range(5):
            clf = get_model(param_grid, x_train=x_train, y_train=y_train, x_test=x_test, y_test=y_test, verbose=0)
            y_pred = clf.predict(x_val)
            acc = metrics.accuracy_score(y_val, y_pred)
            f1_score = metrics.f1_score(y_val, y_pred,
                                        average='weighted')

            accs.append(acc)
            f1s.append(f1_score)
        performances_acc.append(np.mean(accs))
        performances_acc_std.append(np.std(accs))

        performances_f1.append(np.mean(f1s))
        performances_f1_std.append(np.std(f1s))

    pickle.dump([performances_acc, performances_acc_std, performances_f1, performances_f1_std],
                open(f"{get_data_path()}/experiments/ITSC/experiment1/performance.pkl", 'wb'))

    plt.plot(values, performances_acc, label="accuracy")
    plt.fill_between(values, np.array(performances_acc) - np.array(performances_acc_std),
                     np.array(performances_acc) + np.array(performances_acc_std), alpha=.1)

    plt.plot(values, performances_f1, label="f1")
    plt.fill_between(values, np.array(performances_f1) - np.array(performances_f1_std),
                     np.array(performances_f1) + np.array(performances_f1_std), alpha=.1)

    plt.xlabel(f"$r$")
    plt.ylabel(f"score")

    plt.legend()
    plt.savefig(f"{get_data_path()}/plots/time_window_perf.png")

import pickle
import random
from pathlib import Path

from Experiment4.preprocessing import clean_and_interpolate_trajectories
from Experiment_tools.data_import import get_western_med_trajectories
from skais.utils.config import get_data_path

if __name__ == '__main__':
    if __name__ == '__main__':
        random.seed(42)
        trajectories_wm = get_western_med_trajectories()
        trajectories_wm = clean_and_interpolate_trajectories(trajectories_wm)

        trajectories = []
        for trajectory in trajectories_wm:
            if len(trajectory.df.index) > 12:
                trajectories.append(trajectory)

        random.shuffle(trajectories)
        n = len(trajectories)

        train_trajectories = trajectories[:int(n * 0.6)]
        test_trajectories = trajectories[int(n * 0.6):int(n * 0.8)]
        val_trajectories = trajectories[int(n * 0.8):]

        Path(f"{get_data_path()}/experiments/ITSC/data/").mkdir(parents=True, exist_ok=True)
        pickle.dump(train_trajectories, open(f"{get_data_path()}/experiments/ITSC/data/wm_train.pkl", 'wb'))
        pickle.dump(test_trajectories, open(f"{get_data_path()}/experiments/ITSC/data/wm_test.pkl", 'wb'))
        pickle.dump(val_trajectories, open(f"{get_data_path()}/experiments/ITSC/data/wm_val.pkl", 'wb'))
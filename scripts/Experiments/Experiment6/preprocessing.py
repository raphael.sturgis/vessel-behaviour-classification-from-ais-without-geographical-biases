import pickle
import random
from pathlib import Path

import numpy as np
from skais.ais.ais_points import AISPoints
from skais.utils.config import get_data_path
from tqdm import tqdm

from Experiment_tools.data_import import get_baltic_trajectories, get_suez_trajectories

np.seterr(invalid='ignore')

max_instances_per_file = 1000


def clean_and_interpolate_trajectories(trajectories):
    trajectories_split = []
    outputs = []
    for trajectory in trajectories:
        trajectories_split += trajectory.split_trajectory(time_gap=3600, interpolation=300)

    for trajectory in tqdm(trajectories_split, desc='cleaning trajectories', position=0):
        trajectory.remove_outliers(['sog'])
        outputs.append(trajectory)

    return outputs


def dump_trajectories_training(name, tqdm_position=1):
    train_trajectories = pickle.load(open(f"{get_data_path()}/experiments/ITSC/data/wm_train.pkl", 'rb'))
    test_trajectories = pickle.load(open(f"{get_data_path()}/experiments/ITSC/data/wm_test.pkl", 'rb'))
    val_trajectories = pickle.load(open(f"{get_data_path()}/experiments/ITSC/data/wm_val.pkl", 'rb'))

    tmp = []
    for trajectory in train_trajectories:
        shifted = trajectory.shift_trajectory_to_coordinates(
            target_coordinate=(
                random.uniform(-45, 45), random.uniform(-90, 90)
            )
        )
        tmp.append(shifted)
    train_trajectories = tmp

    tmp = []
    for trajectory in test_trajectories:
        shifted = trajectory.shift_trajectory_to_coordinates(
            target_coordinate=(
                random.uniform(-45, 45), random.uniform(-90, 90)
            )
        )
        tmp.append(shifted)
    test_trajectories = tmp

    points = AISPoints.fuse(train_trajectories)
    normalisation_factors = points.normalize(
        standardization_features=['delta_sog', 'std_cog', 'std_hdg', 'std_delta_cog',
                                  'std_delta_heading',
                                  'std_pos', 'std_sog'],
        third_quartile_features=['sog'],
        divide_by_value=[('drift', 180), ('delta_cog', 180), ('delta_heading', 180),
                         ('latitude', 90), ('longitude', 180)]
    )

    for t in train_trajectories:
        t.normalize(normalization_dict=normalisation_factors)

    for t in test_trajectories:
        t.normalize(normalization_dict=normalisation_factors)

    for t in val_trajectories:
        t.normalize(normalization_dict=normalisation_factors)

    pickle.dump(train_trajectories, open(
        f"{get_data_path()}/experiments/ITSC/experiment6/datasets/"
        f"normalization_factors_train.pkl", 'wb'))
    pickle.dump(train_trajectories, open(
        f"{get_data_path()}/experiments/ITSC/experiment6/datasets/"
        f"{name}_eng_train.pkl", 'wb'))
    pickle.dump(test_trajectories, open(
        f"{get_data_path()}/experiments/ITSC/experiment6/datasets/"
        f"{name}_eng_test.pkl", 'wb'))
    pickle.dump(val_trajectories, open(
        f"{get_data_path()}/experiments/ITSC/experiment6/datasets/"
        f"{name}_eng_val.pkl", 'wb'))


def dump_trajectories(trajectories, name):
    t_eng = []
    for trajectory in tqdm(trajectories, desc=name, leave=False, position=1):
        if len(trajectory.df.index) > 12:
            t_eng.append(trajectory)

    pickle.dump(t_eng,
                open(f"{get_data_path()}/experiments/ITSC/experiment6/datasets/{name}_eng.pkl", 'wb'))


if __name__ == '__main__':
    Path(f"{get_data_path()}/experiments/ITSC/experiment6/instances/").mkdir(parents=True, exist_ok=True)
    Path(f"{get_data_path()}/experiments/ITSC/experiment6/datasets/").mkdir(parents=True, exist_ok=True)

    instances = []
    index = 0
    nb_layers = [1, 2, 3, 4]
    nb_units = [5, 10, 20, 50, 100]
    nb_runs = 5
    # generating LSTM instances
    for run in range(nb_runs):
        for nb_layer in nb_layers:
            for nb_unit in nb_units:
                instances.append((run, nb_layer, nb_unit))
                index += 1

    random.shuffle(instances)  # crude workload balancing

    if max_instances_per_file < index:
        splited = []
        len_l = len(instances)
        for i in range(max_instances_per_file):
            start = int(i * len_l / max_instances_per_file)
            end = int((i + 1) * len_l / max_instances_per_file)
            splited.append(instances[start:end])

        for instance_file_number, instances in enumerate(splited):
            f = open(f"{get_data_path()}/experiments/ITSC/experiment6/instances/{instance_file_number}.inst", "w")
            for instance in instances:
                f.write(f"{' '.join(str(e) for e in instance)}\n")
            f.close()
    else:
        for instance_file_number, instance in enumerate(instances):
            f = open(f"{get_data_path()}/experiments/ITSC/experiment6/instances/{instance_file_number}.inst", "w")
            f.write(f"{' '.join(str(e) for e in instance)}\n")
            f.close()

    Path(f"{get_data_path()}/experiments/ITSC/experiment6/models").mkdir(parents=True, exist_ok=True)

    g = open(f"{get_data_path()}/experiments/ITSC/experiment6/results.csv", "w")
    g.write(f"run,nb_layers,nb_units,accuracy,f1 score,recall,precision\n")
    g.close()

    trajectories_baltic = get_baltic_trajectories()
    trajectories_baltic = clean_and_interpolate_trajectories(trajectories_baltic)

    trajectories_suez = get_suez_trajectories()
    trajectories_suez = clean_and_interpolate_trajectories(trajectories_suez)

    dump_trajectories(trajectories_baltic, 'baltic')
    dump_trajectories(trajectories_suez, 'suez')
    dump_trajectories_training('wm')

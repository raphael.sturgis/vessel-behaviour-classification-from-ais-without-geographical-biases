# Experiment 6

This experiment was run on a compute cluster using slurm as a job scheduler

1. Run preprocessing.py
2. Run model_training.py [0-1000]
3. Run exploit_results.py
import pickle
import sys

import pandas as pd
from skais.ais.ais_points import AISPoints
from skais.utils.config import get_data_path

from Experiment_tools.features import FEATURES_LIST
from runner_lstm import RAW, convert_to_vectors, create_model, run_model

if __name__ == '__main__':
    config = int(sys.argv[1])
    # find best lstms
    df = pd.read_csv(f"{get_data_path()}/experiments/ITSC/experiment3/results_lstm.csv")

    mean = df.groupby(['config', 'nb_layers', 'nb_units'], as_index=False).mean()

    best_model = mean.iloc[mean[mean['config'] == config]['accuracy'].idxmax()]
    best_nb_layers = int(best_model['nb_layers'])
    best_nb_units = int(best_model['nb_units'])

    print(config, best_nb_layers, best_nb_units)

    features = None
    if (config - 1) // 2 == 0:
        # raw
        t = pickle.load(open(f"{get_data_path()}/experiments/ITSC/experiment3/data_raw.pkl", 'rb'))

        features = RAW

    elif (config - 1) // 2 == 1:
        # shifted
        t = pickle.load(open(f"{get_data_path()}/experiments/ITSC/experiment3/data_shifted.pkl", 'rb'))

        features = RAW

    elif (config - 1) // 2 == 2:
        # engineered
        t = pickle.load(open(f"{get_data_path()}/experiments/ITSC/experiment3/data_eng.pkl", 'rb'))

        features = FEATURES_LIST
    else:
        t = None
        exit()

    if (config - 1) % 2 == 1:
        points = AISPoints.fuse(t)
        normalisation_factors = points.normalize(
            standardization_features=['delta_sog', 'std_cog', 'std_hdg', 'std_delta_cog',
                                      'std_delta_heading',
                                      'std_pos', 'std_sog'],
            third_quartile_features=['sog'],
            divide_by_value=[('drift', 180), ('delta_cog', 180), ('delta_heading', 180),
                             ('latitude', 90), ('longitude', 180)]
        )
        for trajectory in t:
            trajectory.normalize(normalization_dict=normalisation_factors)

        pickle.dump(normalisation_factors, open(f"{get_data_path()}/experiments/ITSC/experiment3/models/LSTM/normalisation_factors{config}.pkl", 'wb'))

    x_train, y_train = convert_to_vectors(t[:int(0.8 * len(t))], features)
    x_val, y_val = convert_to_vectors(t[:int(0.8 * len(t))], features)

    model = create_model(best_nb_layers, best_nb_units, len(features))

    name = f"best_{config}"
    history, trained_model = run_model(model, x_train, y_train, x_val, y_val, name,
                                       get_history=True)

    pickle.dump(history.history, open(f"{get_data_path()}/experiments/ITSC/experiment3/models/LSTM/history_{config}.pkl", 'wb'))
    trained_model.save(f"{get_data_path()}/experiments/ITSC/experiment3/models/LSTM/model_{config}.h5")
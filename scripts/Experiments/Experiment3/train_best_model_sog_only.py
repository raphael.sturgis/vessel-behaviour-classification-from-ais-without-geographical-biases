import pickle

import pandas as pd
from skais.ais.ais_points import AISPoints
from skais.utils.config import get_data_path

from runner_lstm import RAW, convert_to_vectors, create_model, run_model

if __name__ == '__main__':
    # find best lstms
    df = pd.read_csv(f"{get_data_path()}/experiments/ITSC/experiment3/results_lstm.csv")

    mean = df.groupby(['config', 'nb_layers', 'nb_units'], as_index=False).mean()

    best_model = mean.iloc[mean[mean['config'].str.contains('sog_only')]['accuracy'].idxmax()]
    best_nb_layers = int(best_model['nb_layers'])
    best_nb_units = int(best_model['nb_units'])

    print(best_nb_layers, best_nb_units)

    features = ['sog']
    t = pickle.load(open(f"{get_data_path()}/experiments/ITSC/experiment3/data_raw.pkl", 'rb'))

    points = AISPoints.fuse(t)
    normalisation_factors = points.normalize(
        third_quartile_features=['sog']
    )
    for trajectory in t:
        trajectory.normalize(normalization_dict=normalisation_factors)

    pickle.dump(normalisation_factors, open(f"{get_data_path()}/experiments/ITSC/experiment3/models/LSTM/normalisation_factors_sog_only.pkl", 'wb'))

    x_train, y_train = convert_to_vectors(t[:int(0.8 * len(t))], features)
    x_val, y_val = convert_to_vectors(t[:int(0.8 * len(t))], features)

    model = create_model(best_nb_layers, best_nb_units, len(features))

    name = f"best_sog_only"
    history, trained_model = run_model(model, x_train, y_train, x_val, y_val, name,
                                       get_history=True)

    pickle.dump(history.history, open(f"{get_data_path()}/experiments/ITSC/experiment3/models/LSTM/history_sog_only.pkl", 'wb'))
    trained_model.save(f"{get_data_path()}/experiments/ITSC/experiment3/models/LSTM/model_sog_only.h5")
import pickle
import sys

import numpy as np
from matplotlib import pyplot as plt
from skais.ais.ais_points import AISPoints
from skais.utils.config import get_data_path
from sklearn import metrics, tree
from sklearn.metrics import confusion_matrix, precision_score, accuracy_score, recall_score, f1_score
from sklearn.model_selection import GridSearchCV, ParameterGrid
from sklearn.tree import DecisionTreeClassifier
from tqdm import tqdm

from Experiment_tools.features import FEATURES_LIST

RAW = ['latitude', 'longitude', 'sog']
param_grid = {'criterion': ['gini', 'entropy'], 'splitter': ["best", "random"],
              'max_depth': [1, 2, 5, 10], "class_weight": ["balanced"]}

np.seterr(invalid='ignore')


def get_model(param_grid, x_train, y_train, x_test, y_test, runs=5, verbose=1):
    best_acc = 0
    best_model = None
    if verbose < 1:
        iterator = list(ParameterGrid(param_grid))
    else:
        iterator = tqdm(list(ParameterGrid(param_grid)))
    for params in iterator:
        for _ in range(runs):
            dt = DecisionTreeClassifier(criterion=params['criterion'], splitter=params['splitter'],
                                        max_depth=params['max_depth'], class_weight=params['class_weight'])
            dt.fit(x_train, y_train)
            y_pred = dt.predict(x_test)
            acc = accuracy_score(y_test, y_pred)
            if acc > best_acc:
                best_acc = acc
                best_model = dt
    return best_model


def run_tree_instance(configuration):
    if configuration == 1 or configuration == 2:
        # raw
        train = pickle.load(open(f"{get_data_path()}/experiments/ITSC/experiment3/train_raw.pkl", 'rb'))
        test = pickle.load(open(f"{get_data_path()}/experiments/ITSC/experiment3/test_raw.pkl", 'rb'))
    elif configuration == 3:
        # engineered
        train = pickle.load(open(f"{get_data_path()}/experiments/ITSC/experiment3/train_eng.pkl", 'rb'))
        test = pickle.load(open(f"{get_data_path()}/experiments/ITSC/experiment3/test_eng.pkl", 'rb'))
    else:
        train = None
        test = None
        exit()
    points_train = AISPoints.fuse(train)
    points_test = AISPoints.fuse(test)

    if configuration == 1:
        X_train = points_train.df[['sog']].to_numpy()
        X_test = points_test.df[['sog']].to_numpy()
        f = ['sog']
    elif configuration == 2:
        X_train = points_train.df[RAW].to_numpy()
        X_test = points_test.df[RAW].to_numpy()
        f = RAW
    else:
        X_train = points_train.df[FEATURES_LIST].to_numpy()
        X_test = points_test.df[FEATURES_LIST].to_numpy()
        f = FEATURES_LIST
    y_train = points_train.df['label'].to_numpy()
    y_test = points_test.df['label'].to_numpy()

    best_estimator = get_model(param_grid, X_train, y_train, X_test, y_test)
    fig = plt.figure(figsize=(100, 80))
    _ = tree.plot_tree(best_estimator,
                       feature_names=f,
                       class_names=["docked", 'underway', "moored", "drifting"],
                       filled=True)
    fig.savefig(f"decision_tree_{configuration}.png")

    return best_estimator


if __name__ == '__main__':
    t_wm = pickle.load(open(f"{get_data_path()}/experiments/ITSC/experiment3/val_eng.pkl", 'rb'))
    points_wm = AISPoints.fuse(t_wm)

    t_baltic = pickle.load(open(f"{get_data_path()}/experiments/ITSC/experiment3/baltic_eng.pkl", 'rb'))
    points_baltic = AISPoints.fuse(t_baltic)

    t_suez = pickle.load(open(f"{get_data_path()}/experiments/ITSC/experiment3/suez_eng.pkl", 'rb'))
    points_suez = AISPoints.fuse(t_suez)

    performances_wm = {
        "accuracy": [],
        "precision": [],
        "recall": [],
        "f1": [],
        "confusion_matrix": [],
        "feature_importance": []
    }

    performances_baltic = {
        "accuracy": [],
        "precision": [],
        "recall": [],
        "f1": [],
        "f1_per_class": [],
        "confusion_matrix": [],
        "feature_importance": []
    }

    performances_suez = {
        "accuracy": [],
        "precision": [],
        "recall": [],
        "f1": [],
        "confusion_matrix": [],
        "feature_importance": []
    }

    for config in [1, 2, 3]:
        model = run_tree_instance(config)

        if config == 1:
            X_wm = points_wm.df[['sog']].to_numpy()
            X_baltic = points_baltic.df[['sog']].to_numpy()
            X_suez = points_suez.df[['sog']].to_numpy()
        elif config == 2:
            X_wm = points_wm.df[RAW].to_numpy()
            X_baltic = points_baltic.df[RAW].to_numpy()
            X_suez = points_suez.df[RAW].to_numpy()
        else:
            X_wm = points_wm.df[FEATURES_LIST].to_numpy()
            X_baltic = points_baltic.df[FEATURES_LIST].to_numpy()
            X_suez = points_suez.df[FEATURES_LIST].to_numpy()
        y_wm = points_wm.df['label'].to_numpy()
        y_baltic = points_baltic.df['label'].to_numpy()
        y_suez = points_suez.df['label'].to_numpy()

        y_pred_wm = model.predict(X_wm)
        y_pred_b = model.predict(X_baltic)
        y_pred_s = model.predict(X_suez)

        performances_wm["accuracy"].append(accuracy_score(y_wm, y_pred_wm))
        performances_wm["precision"].append(precision_score(y_wm, y_pred_wm, average='weighted'))
        performances_wm["recall"].append(recall_score(y_wm, y_pred_wm, average='weighted'))
        performances_wm["f1"].append(f1_score(y_wm, y_pred_wm, average='weighted'))
        performances_wm["confusion_matrix"].append(confusion_matrix(y_wm, y_pred_wm))
        performances_wm["feature_importance"].append(model.feature_importances_)

        performances_baltic["accuracy"].append(accuracy_score(y_baltic, y_pred_b))
        performances_baltic["precision"].append(precision_score(y_baltic, y_pred_b, average='weighted'))
        performances_baltic["recall"].append(recall_score(y_baltic, y_pred_b, average='weighted'))
        performances_baltic["f1"].append(f1_score(y_baltic, y_pred_b, average='weighted'))
        performances_baltic["confusion_matrix"].append(confusion_matrix(y_baltic, y_pred_b))
        performances_baltic["feature_importance"].append(model.feature_importances_)

        performances_suez["accuracy"].append(accuracy_score(y_suez, y_pred_s))
        performances_suez["precision"].append(precision_score(y_suez, y_pred_s, average='weighted'))
        performances_suez["recall"].append(recall_score(y_suez, y_pred_s, average='weighted'))
        performances_suez["f1"].append(f1_score(y_suez, y_pred_s, average='weighted'))
        performances_suez["confusion_matrix"].append(confusion_matrix(y_suez, y_pred_s))
        performances_suez["feature_importance"].append(model.feature_importances_)

    print("success!")

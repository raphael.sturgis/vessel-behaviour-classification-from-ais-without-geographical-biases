import fcntl
import pickle
import random
import sys

import numpy as np
from skais.ais.ais_points import AISPoints
from skais.utils.config import get_data_path
from sklearn import metrics
from tensorflow.keras.layers import LSTM, Dense, TimeDistributed, Bidirectional
from tensorflow.keras.models import Sequential
from tensorflow.python.keras.callbacks import EarlyStopping, ModelCheckpoint, ReduceLROnPlateau
from tensorflow.python.keras.saving.save import load_model

from Experiment_tools.features import FEATURES_LIST
from Experiment_tools.generator import MyBatchGenerator

RAW = ['latitude', 'longitude', 'sog']

np.seterr(invalid='ignore')


def create_model(nb_layers, nb_units, nb_inputs):
    model = Sequential()

    model.add(Bidirectional(LSTM(nb_units, return_sequences=True), input_shape=(None, nb_inputs)))
    for _ in range(nb_layers - 1):
        model.add(Bidirectional(LSTM(nb_units, return_sequences=True)))
    model.add(TimeDistributed(Dense(4, activation='softmax')))

    optimizer = "Adam"
    model.compile(loss='categorical_crossentropy',
                  optimizer=optimizer,
                  metrics=['accuracy'])

    # print(model.summary(90))

    return model


def convert_to_vectors(trajectories, features):
    X = []
    Y = []
    for trajectory in trajectories:
        trajectory.remove_outliers(['sog'])
        # trajectories.append(trajectory)
        X.append(trajectory.df[features].to_numpy())
        y = trajectory.df['label'].to_numpy()
        np.zeros((y.size, 4))
        y_one_hot = np.zeros((y.size, 4))
        y_one_hot[np.arange(y.size), y] = 1
        Y.append(y_one_hot)
    return X, Y


def run_model(model, x_train, y_train, x_val, y_val, name, get_history=False):
    early_stopping = EarlyStopping(monitor='val_loss', patience=10, mode='min')
    mcp_save = ModelCheckpoint(
        f"{get_data_path()}/experiments/ITSC/experiment3/models/LSTM/lstm_{name}.hdf5"
        , save_best_only=True, monitor='val_loss',
        mode='min')
    reduce_lr_loss = ReduceLROnPlateau(monitor='val_loss', factor=0.1, patience=7, verbose=0, min_delta=1e-4,
                                       mode='min')
    history = model.fit(MyBatchGenerator(x_train, y_train, batch_size=1),
              validation_data=MyBatchGenerator(x_val, y_val, batch_size=1),
              callbacks=[early_stopping, mcp_save, reduce_lr_loss],
              epochs=100)

    saved_model = load_model(
        f"{get_data_path()}/experiments/ITSC/experiment3/models/LSTM/lstm_{name}.hdf5")

    if get_history:
        return history, saved_model
    return saved_model


def evaluate_model(model, x_test, y_test, run, nb_layers, nb_units, config):
    predictions = []
    for x in x_test:
        predictions.append(model.predict(np.array([x])))

    y_predict = np.concatenate(predictions, axis=1)
    y_true = np.concatenate(y_test)

    acc = metrics.accuracy_score(np.argmax(y_true, axis=1), np.argmax(y_predict[0], axis=1))
    precision = metrics.precision_score(np.argmax(y_true, axis=1), np.argmax(y_predict[0], axis=1), average='weighted')
    recall = metrics.recall_score(np.argmax(y_true, axis=1), np.argmax(y_predict[0], axis=1), average='weighted')
    f1 = metrics.f1_score(np.argmax(y_true, axis=1), np.argmax(y_predict[0], axis=1), average='weighted')

    with open(f"{get_data_path()}/experiments/ITSC/experiment3/results_lstm.csv", "a") as g:
        fcntl.flock(g, fcntl.LOCK_EX)
        g.write(f"{run}, {config}, {nb_layers},{nb_units},{acc},{f1},{recall},{precision}\n")
        fcntl.flock(g, fcntl.LOCK_UN)


def run_lstm_instance(instance):
    config = int(instance[0])
    run = int(instance[1])
    nb_layers = int(instance[2])
    nb_units = int(instance[3])

    features = None
    if (config - 1) // 2 == 0:
        # raw
        t = pickle.load(open(f"{get_data_path()}/experiments/ITSC/experiment3/data_raw.pkl", 'rb'))
        print("loading raw")
        features = RAW

    elif (config - 1) // 2 == 1:
        # shifted
        print("loading shifted data")
        t = pickle.load(open(f"{get_data_path()}/experiments/ITSC/experiment3/data_shifted.pkl", 'rb'))

        features = RAW

    elif (config - 1) // 2 == 2:
        # engineeredok
        t = pickle.load(open(f"{get_data_path()}/experiments/ITSC/experiment3/data_eng.pkl", 'rb'))
        print("loading engineered")

        features = FEATURES_LIST
    else:
        t = None
        exit()

    random.shuffle(t)

    t_train = t[:int(len(t) * .66)]
    t_test = t[int(len(t) * .66):]

    t_train = t_train[:int(len(t_train) * .80)]
    t_val = t_train[int(len(t_train) * .80):]

    if (config - 1) % 2 == 1:
        points = AISPoints.fuse(t_train)
        normalisation_factors = points.normalize(
            standardization_features=['delta_sog', 'std_cog', 'std_hdg', 'std_delta_cog',
                                      'std_delta_heading',
                                      'std_pos', 'std_sog'],
            third_quartile_features=['sog'],
            divide_by_value=[('drift', 180), ('delta_cog', 180), ('delta_heading', 180),
                             ('latitude', 90), ('longitude', 180)]
        )
        for t in t_train:
            t.normalize(normalization_dict=normalisation_factors)

        for t in t_test:
            t.normalize(normalization_dict=normalisation_factors)

        for t in t_val:
            t.normalize(normalization_dict=normalisation_factors)

    x_train, y_train = convert_to_vectors(t_train, features)
    x_test, y_test = convert_to_vectors(t_test, features)
    x_val, y_val = convert_to_vectors(t_val, features)

    model = create_model(nb_layers, nb_units, len(features))

    name = f"training_{run}_{config}_{nb_layers}_{nb_units}"
    model = run_model(model, x_train, y_train, x_val, y_val, name)

    evaluate_model(model, x_test, y_test, run, nb_layers, nb_units, config)

if __name__ == '__main__':
    f = open(f"{get_data_path()}/experiments/ITSC/experiment3/instances/lstm/{sys.argv[1]}.inst")
    lines = f.readlines()
    for line in lines:
        instance = line.split()
        run_lstm_instance(instance)
    print("success!")

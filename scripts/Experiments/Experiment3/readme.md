# Experiment 3

This experiment was run on a compute cluster using slurm as a job scheduler

1. Run preprocessing.py
2. Run preprocessing_sog_only.py
3. Run runner_lstm.py [0-1000]
4. Run runner_lstm_only_sog.py [0-1000]
5. Run runner_svm.py [0-1000]
6. Run runner_tree.py
7. Run train_best_model_per_config.py [1-7]
8. Run train_best_model_sog_only.py
9. Run exploit_results.py
10. Run exploit_results_2.py
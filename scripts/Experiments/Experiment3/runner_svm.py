import fcntl
import pickle
import sys

import numpy as np
from skais.ais.ais_points import AISPoints
from skais.utils.config import get_data_path
from sklearn import metrics
from sklearn.model_selection import GridSearchCV
from sklearn.svm import SVC

from Experiment_tools.features import FEATURES_LIST

RAW = ['latitude', 'longitude', 'sog']
param_grid = {'C': [0.1, 1, 10, 100], 'gamma': [1, 0.1, 0.01], 'kernel': ['rbf', 'poly', 'sigmoid']}

np.seterr(invalid='ignore')


def run_svm_instance(configuration):
    if configuration == 1:
        # raw
        t = pickle.load(open(f"{get_data_path()}/experiments/ITSC/experiment3/data_raw.pkl", 'rb'))

    elif configuration == 2:
        # shifted
        t = pickle.load(open(f"{get_data_path()}/experiments/ITSC/experiment3/data_shifted.pkl", 'rb'))

    elif configuration == 3:
        # engineered
        t = pickle.load(open(f"{get_data_path()}/experiments/ITSC/experiment3/data_eng.pkl", 'rb'))
    else:
        t = None
        exit()

    points = AISPoints.fuse(t)
    normalisation_factors = points.normalize(
        standardization_features=['delta_sog', 'std_cog', 'std_hdg', 'std_delta_cog',
                                  'std_delta_heading',
                                  'std_pos', 'std_sog'],
        third_quartile_features=['sog'],
        divide_by_value=[('drift', 180), ('delta_cog', 180), ('delta_heading', 180),
                         ('latitude', 90), ('longitude', 180)]
    )

    pickle.dump(normalisation_factors,
                open(f"{get_data_path()}/experiments/ITSC/experiment3/models/SVM/{configuration}_normalisation_factors.csv",
                     "wb"))

    if configuration == 1 or configuration == 2:
        X_train = points.df[RAW].to_numpy()
    else:
        X_train = points.df[FEATURES_LIST].to_numpy()

    y_train = points.df['label'].to_numpy()

    grid = GridSearchCV(SVC(), param_grid, refit=True, verbose=2, n_jobs=8)
    grid.fit(X_train, y_train)

    pickle.dump(grid, open(f"{get_data_path()}/experiments/ITSC/experiment3/models/SVM/svm_{configuration}.csv", "wb"))

if __name__ == '__main__':
    config = int(sys.argv[1])
    run_svm_instance(config)
    print("success!")

import random
import numpy as np
from skais.utils.config import get_data_path

np.seterr(invalid='ignore')
if __name__ == '__main__':
    instances = []
    index = 0
    # generating LSTM instances
    for run in range(10):
        for nb_layers in [1, 2, 3, 4]:
            for nb_units in [5, 10, 20, 50, 100]:
                instances.append((run, nb_layers, nb_units))
                index += 1

    random.shuffle(instances)  # crude workload balancing

    for instance_file_number, instance in enumerate(instances):
        f = open(f"{get_data_path()}/experiments/ITSC/experiment3/instances/lstm/{instance_file_number}_sog_only.inst", "w")
        f.write(f"{' '.join(str(e) for e in instance)}\n")
        f.close()

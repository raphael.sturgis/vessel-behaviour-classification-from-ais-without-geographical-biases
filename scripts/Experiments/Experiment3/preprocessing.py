import pickle
import random
from copy import deepcopy
from pathlib import Path
import numpy as np
import tqdm
from skais.utils.config import get_data_path

from Experiment_tools.data_import import get_baltic_trajectories, get_suez_trajectories
from Experiment_tools.features import compute_selected_features

np.seterr(invalid='ignore')

radius = 17
max_instances_per_file = 1000


def dump_trajectories(trajectories, name, split=True):
    if split:
        trajectories_split = []
        for trajectory in trajectories:
            for t in trajectory.split_trajectory(time_gap=3600, interpolation=300):
                if len(t.lax.index) > 12:
                    t.remove_outliers(['sog'])
                    trajectories_split.append(t)
    else:
        trajectories_split = trajectories

    t_raw = []
    t_eng = []

    for trajectory in tqdm.tqdm(trajectories_split):
        t_raw.append(deepcopy(trajectory))
        compute_selected_features(trajectory, radius)
        t_eng.append(trajectory)

    pickle.dump(t_raw, open(f"{get_data_path()}/experiments/ITSC/experiment3/{name}_raw.pkl", 'wb'))
    pickle.dump(t_eng, open(f"{get_data_path()}/experiments/ITSC/experiment3/{name}_eng.pkl", 'wb'))


if __name__ == '__main__':
    Path(f"{get_data_path()}/experiments/ITSC/experiment3/instances/lstm/").mkdir(parents=True, exist_ok=True)

    instances = []
    index = 0
    # generating LSTM instances
    for config in range(1, 7):
        for run in range(10):
            for nb_layers in [1, 2, 3, 4]:
                for nb_units in [5, 10, 20, 50, 100]:
                    instances.append((config, run, nb_layers, nb_units))
                    index += 1

    random.shuffle(instances)  # crude workload balancing

    splited = []
    len_l = len(instances)
    for i in range(max_instances_per_file):
        start = int(i * len_l / max_instances_per_file)
        end = int((i + 1) * len_l / max_instances_per_file)
        splited.append(instances[start:end])

    for instance_file_number, instances in enumerate(splited):
        f = open(f"{get_data_path()}/experiments/ITSC/experiment3/instances/lstm/{instance_file_number}.inst", "w")
        for instance in instances:
            f.write(f"{' '.join(str(e) for e in instance)}\n")
        f.close()

    Path(f"{get_data_path()}/experiments/ITSC/experiment3/models").mkdir(parents=True, exist_ok=True)
    Path(f"{get_data_path()}/experiments/ITSC/experiment3/models/LSTM").mkdir(parents=True, exist_ok=True)
    Path(f"{get_data_path()}/experiments/ITSC/experiment3/models/TREE").mkdir(parents=True, exist_ok=True)

    g = open(f"{get_data_path()}/experiments/ITSC/experiment3/results_lstm.csv", "w")
    g.write(f"run,config,nb_layers,nb_units,accuracy,f1 score,recall,precision\n")
    g.close()

    train_trajectories = pickle.load(open(f"{get_data_path()}/experiments/ITSC/data/wm_train.pkl", 'rb'))
    test_trajectories = pickle.load(open(f"{get_data_path()}/experiments/ITSC/data/wm_test.pkl", 'rb'))
    val_trajectories = pickle.load(open(f"{get_data_path()}/experiments/ITSC/data/wm_val.pkl", 'rb'))
    dump_trajectories(train_trajectories, 'train', split=False)
    dump_trajectories(test_trajectories, 'test', split=False)
    dump_trajectories(val_trajectories, 'val', split=False)

    trajectories = get_baltic_trajectories()
    dump_trajectories(trajectories, 'baltic')

    trajectories = get_suez_trajectories()
    dump_trajectories(trajectories, 'suez')

import pickle
import random
from pathlib import Path

import numpy as np
from skais.ais.ais_points import AISPoints
from skais.utils.config import get_data_path
from tqdm import tqdm

from Experiment_tools.data_import import get_western_med_trajectories, get_baltic_trajectories, get_suez_trajectories
from Experiment_tools.experiment_tools import convert_to_vectors
from Experiment_tools.features import compute_selected_features, FEATURES_LIST

np.seterr(invalid='ignore')

max_instances_per_file = 1000


def clean_and_interpolate_trajectories(trajectories):
    trajectories_split = []
    for trajectory in trajectories:
        trajectories_split += trajectory.split_trajectory(time_gap=3600, interpolation=300)

    for trajectory in tqdm(trajectories_split, desc='cleaning trajectories', position=0):
        trajectory.remove_outliers(['sog'])

    return trajectories_split


def compute_features_on_trajectories(trajectories, window_size, tqdm_position=0):
    trajectories_with_computed_features = []
    for trajectory in tqdm(trajectories, leave=False, position=tqdm_position):
        if len(trajectory.df.index) > 12:
            compute_selected_features(trajectory, window_size)
            trajectories_with_computed_features.append(trajectory)
    return trajectories_with_computed_features


def dump_trajectories_training(name, window_size, tqdm_position=1):
    train_trajectories = pickle.load(open(f"{get_data_path()}/experiments/ITSC/data/wm_train.pkl", 'rb'))
    test_trajectories = pickle.load(open(f"{get_data_path()}/experiments/ITSC/data/wm_test.pkl", 'rb'))
    val_trajectories = pickle.load(open(f"{get_data_path()}/experiments/ITSC/data/wm_val.pkl", 'rb'))

    train_trajectories = compute_features_on_trajectories(train_trajectories, window_size, tqdm_position)
    test_trajectories = compute_features_on_trajectories(test_trajectories, window_size, tqdm_position)
    val_trajectories = compute_features_on_trajectories(val_trajectories, window_size, tqdm_position)

    points = AISPoints.fuse(train_trajectories)
    normalisation_factors = points.normalize(
        standardization_features=['delta_sog', 'std_cog', 'std_hdg', 'std_delta_cog',
                                  'std_delta_heading',
                                  'std_pos', 'std_sog'],
        third_quartile_features=['sog'],
        divide_by_value=[('drift', 180), ('delta_cog', 180), ('delta_heading', 180),
                         ('latitude', 90), ('longitude', 180)]
    )

    for t in train_trajectories:
        t.normalize(normalization_dict=normalisation_factors)

    for t in test_trajectories:
        t.normalize(normalization_dict=normalisation_factors)

    for t in val_trajectories:
        t.normalize(normalization_dict=normalisation_factors)

    pickle.dump(train_trajectories, open(
        f"{get_data_path()}/experiments/ITSC/experiment4/datasets/"
        f"normalization_factors_{window_size}_train.pkl", 'wb'))
    pickle.dump(train_trajectories, open(
        f"{get_data_path()}/experiments/ITSC/experiment4/datasets/"
        f"{name}_eng_{window_size}_train.pkl", 'wb'))
    pickle.dump(test_trajectories, open(
        f"{get_data_path()}/experiments/ITSC/experiment4/datasets/"
        f"{name}_eng_{window_size}_test.pkl", 'wb'))
    pickle.dump(val_trajectories, open(
        f"{get_data_path()}/experiments/ITSC/experiment4/datasets/"
        f"{name}_eng_{window_size}_val.pkl", 'wb'))


def dump_trajectories(trajectories, name, radius):
    t_eng = []
    for trajectory in tqdm(trajectories, desc=name, leave=False, position=1):
        if len(trajectory.df.index) > 12:
            compute_selected_features(trajectory, radius)
            t_eng.append(trajectory)

    pickle.dump(t_eng,
                open(f"{get_data_path()}/experiments/ITSC/experiment4/datasets/{name}_eng_{radius}.pkl", 'wb'))


if __name__ == '__main__':
    Path(f"{get_data_path()}/experiments/ITSC/experiment4/instances/").mkdir(parents=True, exist_ok=True)
    Path(f"{get_data_path()}/experiments/ITSC/experiment4/datasets/").mkdir(parents=True, exist_ok=True)

    instances = []
    index = 0
    radii = [1, 3, 5, 7, 9, 11, 13, 15]
    nb_layers = [1, 2, 3, 4]
    nb_units = [5, 10, 20, 50, 100]
    nb_runs = 5
    # generating LSTM instances
    for run in range(nb_runs):
        for nb_layer in nb_layers:
            for nb_unit in nb_units:
                for window_size in radii:
                    instances.append((run, nb_layer, nb_unit, window_size))
                    index += 1

    random.shuffle(instances)  # crude workload balancing

    if max_instances_per_file < index:
        splited = []
        len_l = len(instances)
        for i in range(max_instances_per_file):
            start = int(i * len_l / max_instances_per_file)
            end = int((i + 1) * len_l / max_instances_per_file)
            splited.append(instances[start:end])

        for instance_file_number, instances in enumerate(splited):
            f = open(f"{get_data_path()}/experiments/ITSC/experiment/instances/{instance_file_number}.inst", "w")
            for instance in instances:
                f.write(f"{' '.join(str(e) for e in instance)}\n")
            f.close()
    else:
        for instance_file_number, instance in enumerate(instances):
            f = open(f"{get_data_path()}/experiments/ITSC/experiment4/instances/{instance_file_number}.inst", "w")
            f.write(f"{' '.join(str(e) for e in instance)}\n")
            f.close()

    Path(f"{get_data_path()}/experiments/ITSC/experiment4/models").mkdir(parents=True, exist_ok=True)

    g = open(f"{get_data_path()}/experiments/ITSC/experiment4/results.csv", "w")
    g.write(f"run,nb_layers,nb_units,window_size,accuracy,f1 score,recall,precision\n")
    g.close()

    trajectories_baltic = get_baltic_trajectories()
    trajectories_baltic = clean_and_interpolate_trajectories(trajectories_baltic)

    trajectories_suez = get_suez_trajectories()
    trajectories_suez = clean_and_interpolate_trajectories(trajectories_suez)

    for window_size in tqdm(radii, leave=True, desc='window size', position=0):
        dump_trajectories(trajectories_baltic, 'baltic', window_size)
        dump_trajectories(trajectories_suez, 'suez', window_size)
        dump_trajectories_training('wm', window_size)

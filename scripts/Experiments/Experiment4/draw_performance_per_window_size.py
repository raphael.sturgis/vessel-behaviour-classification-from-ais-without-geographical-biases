import pickle

import numpy as np
from keras.models import load_model
from skais.utils.config import get_data_path
import pandas as pd
import matplotlib.pyplot as plt

from Experiment_tools.experiment_tools import convert_to_vectors, get_scores
from Experiment_tools.features_list import FEATURES_LIST

radii = [1, 3, 5, 7, 9, 11, 13, 15]

if __name__ == '__main__':
    df = pd.read_csv(f"{get_data_path()}/experiments/ITSC/experiment4/results.csv")

    accuracy_mean = []
    accuracy_std = []
    f1_mean = []
    f1_std = []
    for window_size in radii:
        id_best = df[df['window_size'] == window_size]['accuracy'].idxmax()
        best_config = df.iloc[id_best]

        best_nb_layer = int(best_config['nb_layers'])
        best_nb_units = int(best_config['nb_units'])

        accuracies = []
        f1s = []
        val = pickle.load(
            open(
                f"{get_data_path()}/experiments/ITSC/experiment4/datasets/"
                f"wm_eng_{window_size}_test.pkl", 'rb'))

        x_val, y_val = convert_to_vectors(val, FEATURES_LIST)
        for r in range(5):
            name = f"training_{r}_{best_nb_layer}_{best_nb_units}_{window_size}"
            model = load_model(f"{get_data_path()}/experiments/ITSC/experiment4/models/lstm_{name}.hdf5")
            acc, precision, recall, f1, confusion_matrix = get_scores(model, x_val, y_val)
            accuracies.append(acc)
            f1s.append(f1)

        accuracy_mean.append(np.mean(accuracies))
        accuracy_std.append(np.std(accuracies))

        f1_mean.append(np.mean(f1s))
        f1_std.append(np.std(f1s))

    plt.plot(radii, accuracy_mean, label="accuracy")
    plt.fill_between(radii, np.array(accuracy_mean) - np.array(accuracy_std),
                     np.array(accuracy_mean) + np.array(accuracy_std), alpha=.1)

    plt.plot(radii, f1_std, label="f1")
    plt.fill_between(radii, np.array(f1_std) - np.array(f1_std),
                     np.array(f1_std) + np.array(f1_std), alpha=.1)

    plt.xlabel(f"$r$")
    plt.ylabel(f"score")
    plt.legend()
    plt.savefig(f"{get_data_path()}/plots/time_window_perf_lstm.png")
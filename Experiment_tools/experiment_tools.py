import numpy as np
import tqdm as tqdm
from skais.ais.ais_points import AISPoints
from sklearn import metrics


def make_feature_vectors(trajectories, features=None,
                         label_field='label', nb_classes=2, sliding_window_gap=10, length_list=15, verbose=0):
    if features is None:
        features = ['rot', 'sog', 'd_sog', 'd_cog', 'd_heading', 'd_rot']
    x = []
    y = []
    features_trajectories = []
    label_trajectories = []
    zero = [0 for _ in range(nb_classes)]

    iterator = trajectories
    if verbose > 0:
        iterator = tqdm.tqdm(trajectories)
    for trajectory in iterator:
        if len(trajectory.df.index) > length_list:
            trajectory.df['ts'] = trajectory.df.index
            windows = trajectory.sliding_window(length_list, offset=sliding_window_gap,
                                                fields=features + [label_field])

            features_trajectories.append(trajectory.to_numpy(fields=features))
            label_trajectories.append(trajectory.to_numpy(fields=[label_field]).astype(int))

            if length_list > 0:
                for window in windows:
                    label = zero.copy()
                    label[int(window[length_list // 2, -1])] = 1
                    x.append(window[:, :-1])
                    y.append(label)

    return features_trajectories, label_trajectories, x, y


def match_prediction_to_trajectories(trajectories, prediction, name='prediction'):
    index = 0
    for trajectory in trajectories:
        trajectory.df[name] = prediction[index:index + len(trajectory.df.index)]
        index += len(trajectory.df.index)

    return trajectories


def normalize_set_of_trajectories(trajectories, features):
    points = AISPoints.fuse(trajectories)

    _, normalization_dict = points.normalize(features, "standardization")

    for t in trajectories:
        t.normalize(features, normalization_dict=normalization_dict)


def split_in_time_windows(x, y, size=10, offset=1, label_position=5):
    assert (len(x) == len(y))
    assert offset > 0

    x_windows = []
    y_windows = []
    for tx, ty in zip(x, y):
        assert len(tx) == len(ty)
        index = 0
        while index + size < len(tx):
            x_windows.append(tx[index:index + size])
            y_windows.append(ty[index + label_position])
            index += offset

    return x_windows, y_windows


def convert_to_vectors(trajectories, features):
    X = []
    Y = []
    for trajectory in trajectories:
        trajectory.remove_outliers(['sog'])
        X.append(trajectory.df[features].to_numpy())
        y = trajectory.df['label'].to_numpy()
        np.zeros((y.size, 4))
        y_one_hot = np.zeros((y.size, 4))
        y_one_hot[np.arange(y.size), y] = 1
        Y.append(y_one_hot)
    return X, Y


def get_scores(model, x_test, y_test):
    predictions = []
    for x in tqdm.tqdm(x_test):
        predictions.append(model.predict(np.array([x])))

    y_predict = np.concatenate(predictions, axis=1)[0]
    y_true = np.concatenate(y_test)

    return get_scores_label(np.argmax(y_true, axis=1), np.argmax(y_predict, axis=1))


def get_scores_label(y_true, y_predict):
    acc = metrics.accuracy_score(y_true, y_predict)
    precision = metrics.precision_score(y_true, y_predict,
                                        average='weighted')
    recall = metrics.recall_score(y_true, y_predict, average='weighted')
    f1 = metrics.f1_score(y_true, y_predict, average='weighted')

    confusion_matrix = metrics.confusion_matrix(y_true, y_predict)

    return acc, precision, recall, f1, confusion_matrix

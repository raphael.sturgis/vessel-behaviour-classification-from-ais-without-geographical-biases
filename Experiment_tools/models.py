import math

from tensorflow.keras import Sequential
from tensorflow.keras.layers import Conv1D, MaxPooling1D, ReLU, Flatten, Dense, BatchNormalization


def add_batch_norm_relu_conv_block(m, nb_filters=1, kernel_size=1):
    m.add(BatchNormalization())
    m.add(ReLU())
    m.add(Conv1D(filters=nb_filters, kernel_size=kernel_size))


def create_cnn(nb_blocks=1, nb_sub_block=1, kernel_size=3, nb_filters=10, nb_classes=4, maxpool_kernel_size=4):
    model = Sequential()

    for i in range(nb_blocks):
        for _ in range(nb_sub_block):
            add_batch_norm_relu_conv_block(model, nb_filters, kernel_size)
        model.add(MaxPooling1D(pool_size=maxpool_kernel_size))
    model.add(Flatten())

    model.add(Dense(nb_classes, activation='softmax'))
    return model


def calculate_output_size_block(input_size, nb_sub_block=1, kernel_size=3, maxpool_kernel_size=4):
    return math.floor((input_size - (nb_sub_block * (math.floor(kernel_size / 2) * 2))) / maxpool_kernel_size)


def calculate_output_size(input_size, nb_blocks=1, nb_sub_block=1, kernel_size=3, maxpool_kernel_size=4):
    result = input_size
    for _ in range(nb_blocks):
        result = calculate_output_size_block(result, nb_sub_block, kernel_size, maxpool_kernel_size)
    return result

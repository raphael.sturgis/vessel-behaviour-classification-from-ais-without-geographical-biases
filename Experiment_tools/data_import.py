import pandas as pd

from skais.ais.ais_points import AISPoints
from skais.process.ais_operations import get_trajectories
from skais.utils.config import get_data_path


def get_western_med_trajectories():
    file = f"{get_data_path()}/labeled_western_med.csv"

    df = pd.read_csv(file)
    points = AISPoints(df)
    points.remove_outliers(['sog'])
    points.df['ts_sec'] = points.df['ts'].astype('int64') // 1e9
    return get_trajectories(points)


def get_baltic_trajectories():
    df = pd.read_csv(f"{get_data_path()}/labeled_baltic.csv")

    points = AISPoints(df)
    points.remove_outliers(['sog'])
    points.df['ts_sec'] = points.df['ts'].astype('int64') // 1e9
    return get_trajectories(points)


def get_suez_trajectories():
    df = pd.read_csv(f"{get_data_path()}/Suez_dataset/labeled_suez.csv")

    points = AISPoints(df)
    points.remove_outliers(['sog'])
    points.df['ts_sec'] = points.df['ts'].astype('int64') // 1e9
    return get_trajectories(points)

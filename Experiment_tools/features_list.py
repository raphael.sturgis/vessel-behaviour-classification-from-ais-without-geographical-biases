FEATURES_LIST = ['sog', 'drift', 'delta_sog', 'delta_cog', 'delta_heading', 'std_cog',
                 'std_hdg', 'std_delta_cog', 'std_delta_heading', 'std_pos', 'std_sog']

RAW = ['latitude', 'longitude', 'sog']